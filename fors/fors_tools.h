/* $Id: fors_tools.h,v 1.7 2010-09-14 07:49:30 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2010-09-14 07:49:30 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_TOOLS_H
#define FORS_TOOLS_H

#include <fors_star.h>
#include <fors_setting.h>
#include <fors_image.h>
#include <cpl.h>

CPL_BEGIN_DECLS

double
fors_star_ext_corr(fors_star_list *stars, 
                   const fors_setting *setting,
                   double ext_coeff,
		   double dext_coeff,
                   const cpl_frame *raw_frame);

cpl_table *
fors_create_sources_table(fors_star_list *sources);

double
fors_fixed_pattern_noise(const fors_image *master,
                         double convert_ADU,
                         double master_noise);
double
fors_fixed_pattern_noise_bias(const fors_image *first_raw,
                              const fors_image *second_raw,
                              double ron);
double
fors_get_airmass(const cpl_propertylist *header);

int fors_isnan(double x);

CPL_END_DECLS

#endif
