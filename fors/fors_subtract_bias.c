/*
 * This file is part of the FORS Data Reduction Pipeline
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "fors_utils.h"
#include "fors_image.h"


#undef cleanup
#define cleanup 
/**
 * Subtract a master bias from an image
 * @param image
 * @param bias
 */
void fors_subtract_bias(fors_image * image,
                        const fors_image *bias)
{
    /* Variance is now (image_noise**2  +  bias_noise**2) */
    fors_image_subtract(image, bias);

    assure( !cpl_error_get_code(), return ,  "Bias subtraction failed" );

}

void fors_subtract_bias_imglist(fors_image_list * imglist,
                                const fors_image *bias)
{
    
    fors_image * img = fors_image_list_first(imglist);
    for(int i = 0; i< fors_image_list_size(imglist); i++)
    {
        fors_subtract_bias(img, bias);
        img =  fors_image_list_next(imglist);
    }

    assure( !cpl_error_get_code(), return ,  "Bias subtraction failed" );

}
