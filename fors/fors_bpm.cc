/*
 * This file is part of the FORS Data Reduction Pipeline
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/*
 * fors_bpm.cc
 *
 *  Created on: 2014 3 7
 *      Author: cgarcia
 */

#include <fors_bpm.h>
#include "cpl.h"

cpl_image * fors_bpm_create_combined_bpm
(cpl_mask ** non_linear_masks,
 cpl_mask ** saturated_masks,
 size_t nmasks)
{
    cpl_image * combined_bpm;

    /* Check sizes */
    cpl_size nx = cpl_mask_get_size_x(non_linear_masks[0]); 
    cpl_size ny = cpl_mask_get_size_y(non_linear_masks[0]); 
    for(size_t i_mask = 1; i_mask < nmasks; i_mask++)
        if(nx != cpl_mask_get_size_x(non_linear_masks[0]) ||
           nx != cpl_mask_get_size_x(non_linear_masks[0]) ||    
           ny != cpl_mask_get_size_y(saturated_masks[0]) ||
           ny != cpl_mask_get_size_y(saturated_masks[0]))
            return NULL;

    /* Combine all the same type masks */
    cpl_mask * combined_non_linear = cpl_mask_new(nx, ny);
    cpl_mask * combined_saturated  = cpl_mask_new(nx, ny);
    for(size_t i_mask = 0; i_mask < nmasks; i_mask++)
    {
        cpl_mask_or(combined_non_linear, non_linear_masks[i_mask]);
        cpl_mask_or(combined_saturated, saturated_masks[i_mask]);
    }

    /* Get the combined mask */
    combined_bpm = cpl_image_new(nx, ny, CPL_TYPE_INT);
    cpl_image * combined_non_linear_img =
            cpl_image_new_from_mask(combined_non_linear);
    cpl_image * combined_saturated_img =
            cpl_image_new_from_mask(combined_saturated);
    /* Code for non-linear pixels: 32768 */
    cpl_image_multiply_scalar(combined_non_linear_img, 32768);
    /* Code for saturated pixels: 4096 */
    cpl_image_multiply_scalar(combined_saturated_img, 4096);
    cpl_image_add(combined_bpm, combined_non_linear_img);
    cpl_image_add(combined_bpm, combined_saturated_img);

    /* Cleanup */
    cpl_image_delete(combined_non_linear_img);
    cpl_image_delete(combined_saturated_img);
    cpl_mask_delete(combined_non_linear);
    cpl_mask_delete(combined_saturated);
    
    return combined_bpm;
    
}

cpl_image * fors_bpm_create_combined_bpm
(cpl_mask * non_linear_mask,
 cpl_mask * saturated_mask)
{

    cpl_mask ** non_linear_mask_list = (cpl_mask **)cpl_malloc(1 * sizeof(cpl_mask *));
    cpl_mask ** saturated_mask_list = (cpl_mask **)cpl_malloc(1 * sizeof(cpl_mask *));
    
    non_linear_mask_list[0] = non_linear_mask; 
    saturated_mask_list[0] = saturated_mask; 

    cpl_image * combined_bpm =
      fors_bpm_create_combined_bpm(non_linear_mask_list, saturated_mask_list, 1);
    
    cpl_free(non_linear_mask_list);
    cpl_free(saturated_mask_list);
    
    return combined_bpm;
}


/**
 * This function will ensure that each of the input images in the list has
 * a explicit bpm. If the bpm were NULL, one is created with all the pixels
 * set to valid 
 * @param ima_list  The input image list
 */
void fors_bpm_image_list_make_explicit(fors_image_list * ima_list)
{
    int nima = fors_image_list_size(ima_list);

    const fors_image * target_ima = fors_image_list_first_const(ima_list);
    for(int ima = 0; ima < nima; ++ima)
    {
        fors_bpm_image_make_explicit(target_ima);
        target_ima = fors_image_list_next_const(ima_list);
    }

    return;
}

/**
 * This function will ensure that the input images has
 * a explicit bpm. If the bpm were NULL, one is created with all the pixels
 * set to valid 
 * @param ima_list  The input image list
 */
void fors_bpm_image_make_explicit(const fors_image * ima)
{
    cpl_size dim_x = cpl_image_get_size_x(ima->data);
    cpl_size dim_y = cpl_image_get_size_y(ima->data);
    if(cpl_image_get_bpm_const(ima->data) == NULL)
    {
        cpl_mask * this_mask = cpl_mask_new(dim_x, dim_y);
        cpl_image_set_bpm(ima->data, this_mask);
    }
    if(cpl_image_get_bpm_const(ima->variance) == NULL)
    {
        cpl_mask * this_mask = cpl_mask_new(dim_x, dim_y);
        cpl_image_set_bpm(ima->variance, this_mask);
    }

    return;
}
