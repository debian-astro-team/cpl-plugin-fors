/* $Id: fors_img_screen_flat-test.c,v 1.17 2008-08-07 09:38:01 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2008-08-07 09:38:01 $
 * $Revision: 1.17 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_img_screen_flat_impl.h>
#include <fors_dfs.h>
#include <fors_pfits.h>
#include <fors_utils.h>

#include <test_simulate.h>
#include <test.h>
#include <fs_test.h>

#include <cpl.h>
#include <math.h>

/**
 * @defgroup fors_img_screen_flat_test  test of screen flat recipe
 */

/**@{*/

#undef cleanup
#define cleanup \
do { \
    cpl_frameset_delete(frames); \
    cpl_parameterlist_delete(parameters); \
    fors_image_delete(&raw_sflat); \
    fors_image_delete(&master_sflat); \
    fors_image_delete(&master_bias); \
    fors_setting_delete(&setting); \
    cpl_propertylist_delete(product_header); \
    delete_test_files(screen_flat_filename);\
    delete_test_file(master_name);\
    delete_test_file_from_tag(MASTER_SCREEN_FLAT_IMG);\
    delete_test_file_from_tag(MASTER_NORM_FLAT_IMG);\
} while(0)

/**
 * @brief   Test screen flat recipe
 */
static void
test_img_screen_flat(void)
{
    /* Input */
    cpl_frameset *frames = cpl_frameset_new();
    cpl_parameterlist *parameters = cpl_parameterlist_new();

    /* Output */
    fors_image *raw_sflat = NULL;
    fors_image *master_sflat = NULL;
    fors_image *master_bias = NULL;
    fors_setting *setting = NULL;
    cpl_propertylist *product_header = NULL;

    /* Simulate data */
    const char *screen_flat_filename[] = {"img_screen_flat_1.fits",
                                          "img_screen_flat_2.fits",
                                          "img_screen_flat_3.fits"};

    const char* master_name = "img_screen_flat_master_bias.fits";
    const unsigned N =  sizeof(screen_flat_filename)/sizeof(char *);

    {
        unsigned i;
        
        for (i = 0; i < N; i++) {
            cpl_frameset_insert(
                frames,
                create_screen_flat(screen_flat_filename[i],
                                   SCREEN_FLAT_IMG, CPL_FRAME_GROUP_RAW));
        }
    }
    
    cpl_frameset_insert(frames, 
                        create_master_bias(master_name,
                                    MASTER_BIAS, CPL_FRAME_GROUP_CALIB));
    
    /* Define parameters */
    fors_img_screen_flat_define_parameters(parameters);

    assure( !cpl_error_get_code(), return, 
            "Create parameters failed");
    
    fors_parameterlist_set_defaults(parameters);

    cpl_parameter_set_int(cpl_parameterlist_find(parameters, 
                          "fors.fors_img_screen_flat.xradius"),
                          1);
    cpl_parameter_set_int(cpl_parameterlist_find(parameters, 
                          "fors.fors_img_screen_flat.yradius"),
                          1);

    cpl_parameter_set_int(cpl_parameterlist_find(parameters,
                          "fors.fors_img_screen_flat.degree"),
                          -1);

    /* Call recipe */
    fors_img_screen_flat(frames, parameters);
    assure( !cpl_error_get_code(), return, 
            "Execution error");
    
    /* Test results */

    /* Existence */
    const char *const product_tags[] = {MASTER_SCREEN_FLAT_IMG,
                                        MASTER_NORM_FLAT_IMG};
    const char *const qc[] =  {"QC OVEREXPO",
                               "QC FLAT EFF",
                               "QC FLAT PHN",
                               "QC FLAT FPN",
                               "QC FLAT CONAD",
                               "QC FLAT CONADERR"};
    
    test_recipe_output(frames,
                       product_tags, sizeof product_tags / sizeof *product_tags,
		       MASTER_NORM_FLAT_IMG,
                       qc, sizeof qc / sizeof *qc);    


    setting = fors_setting_new(cpl_frameset_find(frames,
                                                 SCREEN_FLAT_IMG));
    
    {
        /* New and previous frames */
        cpl_test( cpl_frameset_find(frames, MASTER_SCREEN_FLAT_IMG) != NULL );
        cpl_test( cpl_frameset_find(frames, MASTER_NORM_FLAT_IMG) != NULL );
        cpl_test( cpl_frameset_find(frames, MASTER_BIAS) != NULL );
        cpl_test( cpl_frameset_find(frames, SCREEN_FLAT_IMG) != NULL );
        
        master_sflat = fors_image_load(
            cpl_frameset_find(frames, MASTER_NORM_FLAT_IMG));
        master_bias  = fors_image_load(
            cpl_frameset_find(frames, MASTER_BIAS));
        
        raw_sflat    = fors_image_load(
            cpl_frameset_find(frames, SCREEN_FLAT_IMG));
        cpl_image_subtract_scalar(raw_sflat->data, 200); //Hard-coded bias level 
        //fors_subtract_bias(raw_sflat, master_bias); //Sizes are different now that trimming is done before
        
        /* Verify that relative error decreased  */
        cpl_test( fors_image_get_error_mean(master_sflat, NULL) /
              fors_image_get_mean(master_sflat, NULL) 
              <
              fors_image_get_error_mean(raw_sflat, NULL) /
              fors_image_get_mean(raw_sflat, NULL));

        /* Verify normalization */
        cpl_test_rel( fors_image_get_mean(master_sflat, NULL),
                  1.0, 0.01);

        /* Test QC values */
        product_header = 
            cpl_propertylist_load(cpl_frame_get_filename(
                                      cpl_frameset_find(frames,
                                                        MASTER_NORM_FLAT_IMG)),
                                  0);
        assure( product_header != NULL, return, NULL );

        
        cpl_test_rel( cpl_propertylist_get_double(product_header,
                                              "ESO QC FLAT EFF"),
                  fors_image_get_median(raw_sflat, NULL) / setting->exposure_time,
                  0.03 );

        /* QC.FLAT.PHN is the master flat photon noise in ADU.
           But the master flat is normalized to one, so
           multiply by the normalization factor */
/*  %%%
        cpl_test_rel( cpl_propertylist_get_double(product_header,
                                              "ESO QC FLAT PHN"),
                  fors_image_get_stdev(master_sflat, NULL) *
                  fors_image_get_median(raw_sflat, NULL),
                  0.1 );
%%% */


        /* CONAD */
        cpl_test_rel( cpl_propertylist_get_double(product_header,
                                              "ESO QC FLAT CONAD"),
                  cpl_propertylist_get_double(product_header,
                                              FORS_PFITS_CONAD[0]),
                  /* Compare with propagated CONAD value */
                  0.1);

        cpl_test( cpl_propertylist_get_double(product_header,
                                          "ESO QC FLAT CONADERR") <
              0.1 * cpl_propertylist_get_double(product_header,
                                                "ESO QC FLAT CONAD") );
    }
    
    cleanup;
    return;
}

/**
 * @brief   Test of image module
 */
int main(void)
{
    TEST_INIT;
    
    test_img_screen_flat();

    TEST_END;
}

/**@}*/
