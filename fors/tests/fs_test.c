#include "fs_test.h"

#include <stdio.h>
#include <fors_dfs.h>

void cleanup_files(const char* fnames[], size_t n_files)
{
	for(size_t i = 0; i < n_files; i++)
	{
		remove(fnames[i]);
	}
}

void delete_test_file(const char* fname)
{
	remove(fname);
}


void delete_test_file_from_tag(const char* tag)
{
	char *s = dfs_generate_filename(tag);
	delete_test_file(s);
	cpl_free(s);
}

void cleanup_tags(const char* tags[], size_t n_files)
{
	for(size_t i = 0; i < n_files; i++)
	{
		delete_test_file_from_tag(tags[i]);
	}
}
