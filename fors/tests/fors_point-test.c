/* $Id: fors_point-test.c,v 1.1 2007-08-28 13:33:09 jmlarsen Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: jmlarsen $
 * $Date: 2007-08-28 13:33:09 $
 * $Revision: 1.1 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_point.h>
#include <test.h>

/**
 * @defgroup fors_point_test   fors_point unit tests
 */

/**@{*/

/**
 * @brief   test
 */
static void
test_point(void)
{
    fors_point *p1 = fors_point_new(1, 2);
    fors_point *p2 = fors_point_new(2, 2);

    cpl_test_abs( fors_point_distsq(p1, p2), 1.0, 0.01 );
    
    fors_point_delete(&p1);
    fors_point_delete(&p2);

    return;
}


/**
 * @brief   Test of QC module
 */
int main(void)
{
    TEST_INIT;

    test_point();

    TEST_END;
}

/**@}*/
