/* $Id: fors_std_star-test.c,v 1.22 2009-03-26 20:23:48 hlorch Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: hlorch $
 * $Date: 2009-03-26 20:23:48 $
 * $Revision: 1.22 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_data.h>
#include <fors_dfs.h>
#include <fors_instrument.h>
#include <fors_std_cat.h>

#include <test_simulate.h>
#include <test.h>

#include <cpl.h>
#include <math.h>

#include <fs_test.h>

/**
 * @defgroup fors_std_cat_test  Standard star catalogue tests
 */

/**@{*/


#undef cleanup
#define cleanup \
do { \
    cpl_frameset_delete(cat_frames); \
    cpl_frame_delete(raw_frame); \
    cpl_frame_delete(phot_table); \
    fors_std_star_list_delete(&cat, fors_std_star_delete); \
    fors_setting_delete(&setting); \
    cpl_propertylist_delete(raw_header); raw_header = NULL; \
    delete_test_file(filename);\
    delete_test_file(cat_raw_fname);\
    delete_test_file(cat_pho_fname);\
} while (0)

/**
 * @brief  Test loading catalogue
 */
static void
test_new(void)
{
    cpl_frameset *cat_frames = NULL;
    cpl_frame *phot_table = NULL;
    const char *filename = "std_cat.fits";
    const char *cat_raw_fname = "std_cat_raw.fits";
	const char *cat_pho_fname = "std_cat_phot_table.fits";
    fors_std_star_list *cat = NULL;
    fors_setting *setting = NULL;
    cpl_propertylist    *raw_header = NULL;
    double color_term, dcolor_term;
    double ext_coeff, dext_coeff;
    double expected_zeropoint, dexpected_zeropoint;
    char    band;

    /* Simulate */
    cpl_frame *raw_frame = create_standard(cat_raw_fname,
                                           STANDARD_IMG, CPL_FRAME_GROUP_RAW);
    
    phot_table = create_phot_table(cat_pho_fname,
                                   PHOT_TABLE, CPL_FRAME_GROUP_CALIB);
    
    cat_frames = cpl_frameset_new();
    cpl_frameset_insert(cat_frames,
                        create_std_cat(filename, 
                                       FLX_STD_IMG, CPL_FRAME_GROUP_CALIB));

    setting = fors_setting_new(raw_frame);

    fors_phot_table_load(phot_table, setting,
                         &color_term, &dcolor_term,
			 &ext_coeff, &dext_coeff,
			 &expected_zeropoint, &dexpected_zeropoint);
    
    /* Call function */
    band = fors_instrument_filterband_get_by_setting(setting);
    cat = fors_std_cat_load(                cat_frames,
                                            band,
                                            0,
                                            color_term,
                                            dcolor_term);
    raw_header = cpl_propertylist_load(cpl_frame_get_filename(raw_frame), 0);
    fors_std_star_list_apply_wcs(cat, raw_header);
    
    /* Test results (TBI) */
    
    fors_std_star_print_list(CPL_MSG_DEBUG, cat);
    fors_std_star_print_list(CPL_MSG_INFO, cat);

    cleanup;
    return;
}


static void
test_dist(void)
{
    double ra  = 34.5;
    double dec = -0.4;
    double mag = 15;
    double dmag = 0.51;
    double cmag = 15.2;
    double dcmag = 0.21;
    double color = 0.2;
    double dcolor = dcmag;
    double cov_catm_col = dcolor;

    double shift_arcsecs;

    for (shift_arcsecs = 0.1; shift_arcsecs <= 100; shift_arcsecs *= 2) {
        
        fors_std_star *s = fors_std_star_new(ra, dec, mag, dmag, 
					     cmag, dcmag,
                         color, dcolor,
                         cov_catm_col,
                         "some");
        fors_std_star *t = fors_std_star_new(ra  + shift_arcsecs/3600,
                                             dec + shift_arcsecs/3600,
                                             mag, dmag, 
					     cmag, dcmag,
					     color, dcolor,
                         cov_catm_col,
                         "star");
        
        /* At arcsecond scale and DEC ~= 0, RA and DEC directions
           are very perpendicular, so
           expect distance = sqrt(2) * shift_arcsecs 
        */
        cpl_test_rel( fors_std_star_dist_arcsec(s, t), sqrt(2)*shift_arcsecs, 0.01 );
        
        fors_std_star_delete(&s);
        fors_std_star_delete(&t);
    }

    return;
}

/**
 * @brief   Test of QC module
 */
int main(void)
{
    TEST_INIT;

    /* cpl_msg_set_level(CPL_MSG_DEBUG); */
    test_new();

    test_new();  /* The second call tests the reentrancy of WCSLIB and flex,
                    which is non-trivial */

    test_dist();

    TEST_END;
}

/**@}*/
