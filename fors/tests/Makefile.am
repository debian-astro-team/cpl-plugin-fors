## Process this file with automake to produce Makefile.in

##   This file is part of the UVES Pipeline Library
##   Copyright (C) 2002,2003 European Southern Observatory

##   This library is free software; you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation; either version 2 of the License, or
##   (at your option) any later version.

##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.

##   You should have received a copy of the GNU General Public License
##   along with this program; if not, write to the Free Software
##   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

AUTOMAKE_OPTIONS = 1.8 foreign

DISTCLEANFILES = *~ .logfile


if MAINTAINER_MODE

MAINTAINERCLEANFILES = Makefile.in

endif

AM_CPPFLAGS = $(HDRL_INCLUDES) $(all_includes)

pkginclude_HEADERS =
noinst_HEADERS = test.h test_simulate.h fs_test.h
noinst_LTLIBRARIES = $(LIBTEST) $(LIBSIMULATE) $(LIBFSTEST)

# Convenience libraries
LIBTEST = libtest.la
libtest_la_SOURCES =  test.c
libtest_la_LIBADD =
libtest_la_DEPENDENCIES =

LIBSIMULATE = libsimulate.la
libsimulate_la_SOURCES =  test_simulate.c
libsimulate_la_LIBADD =
libsimulate_la_DEPENDENCIES =

LIBFSTEST = libfstest.la
libfstest_la_SOURCES =  fs_test.c
libfstest_la_LIBADD =
libfstest_la_DEPENDENCIES =

# Test programs
check_PROGRAMS = fors-test \
                 list-test \
                 fors_point-test \
                 fors_pattern-test \
                 fors_qc-test \
                 fors_cpl_wcs-test \
                 fors_setting-test \
                 fors_image-test \
                 fors_star-test \
                 fors_std_star-test \
                 fors_identify-test \
                 fors_stack-test \
                 fors_bias-test \
                 fors_dark-test \
                 fors_img_screen_flat-test \
                 fors_img_sky_flat-test \
                 fors_zeropoint-test \
                 fors_photometry-test \
                 fors_img_science-test \
                 fors_dfs_idp-test \
                 fors_dfs-test \
                 fors_trimm_non_illum-test

list_test_SOURCES = list-test.c
list_test_LDFLAGS = $(CPL_LDFLAGS)
list_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS)

fors_test_SOURCES = fors-test.c
fors_test_LDFLAGS = $(CPL_LDFLAGS)
fors_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS)

fors_point_test_SOURCES = fors_point-test.c
fors_point_test_LDFLAGS = $(CPL_LDFLAGS)
fors_point_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS)

fors_pattern_test_SOURCES = fors_pattern-test.c
fors_pattern_test_LDFLAGS = $(CPL_LDFLAGS)
fors_pattern_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) 

fors_qc_test_SOURCES = fors_qc-test.c
fors_qc_test_LDFLAGS = $(CPL_LDFLAGS)
fors_qc_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) 

fors_cpl_wcs_test_SOURCES = fors_cpl_wcs-test.c
fors_cpl_wcs_test_LDFLAGS = $(CPL_LDFLAGS)
fors_cpl_wcs_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) 

fors_image_test_SOURCES = fors_image-test.c
fors_image_test_LDFLAGS = $(CPL_LDFLAGS)
fors_image_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) $(LIBSIMULATE) $(LIBFSTEST)

fors_setting_test_SOURCES = fors_setting-test.c
fors_setting_test_LDFLAGS = $(CPL_LDFLAGS)
fors_setting_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) $(LIBSIMULATE) $(LIBFSTEST)

fors_stack_test_SOURCES = fors_stack-test.c
fors_stack_test_LDFLAGS = $(CPL_LDFLAGS)
fors_stack_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) $(LIBSIMULATE) $(LIBFSTEST)

fors_std_star_test_SOURCES = fors_std_star-test.c
fors_std_star_test_LDFLAGS = $(CPL_LDFLAGS)
fors_std_star_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) $(LIBSIMULATE) $(LIBFSTEST)

fors_star_test_SOURCES = fors_star-test.c
fors_star_test_LDFLAGS = $(CPL_LDFLAGS)
fors_star_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) $(LIBSIMULATE) $(LIBFSTEST)

fors_identify_test_SOURCES = fors_identify-test.c
fors_identify_test_LDFLAGS = $(CPL_LDFLAGS)
fors_identify_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) $(LIBSIMULATE) $(LIBFSTEST)

fors_bias_test_SOURCES = fors_bias-test.c
fors_bias_test_LDFLAGS = $(CPL_LDFLAGS)
fors_bias_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) $(LIBSIMULATE) $(LIBFSTEST)

fors_dfs_test_SOURCES = fors_dfs-test.c
fors_dfs_test_LDFLAGS = $(CPL_LDFLAGS)
fors_dfs_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) $(LIBSIMULATE) $(LIBFSTEST)

fors_dark_test_SOURCES = fors_dark-test.c
fors_dark_test_LDFLAGS = $(CPL_LDFLAGS)
fors_dark_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) $(LIBSIMULATE) $(LIBFSTEST)

fors_img_screen_flat_test_SOURCES = fors_img_screen_flat-test.c
fors_img_screen_flat_test_LDFLAGS = $(CPL_LDFLAGS)
fors_img_screen_flat_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) $(LIBSIMULATE) $(LIBFSTEST)

fors_img_sky_flat_test_SOURCES = fors_img_sky_flat-test.c
fors_img_sky_flat_test_LDFLAGS = $(CPL_LDFLAGS)
fors_img_sky_flat_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) $(LIBSIMULATE) $(LIBFSTEST)

fors_zeropoint_test_SOURCES = fors_zeropoint-test.c
fors_zeropoint_test_LDFLAGS = $(CPL_LDFLAGS)
fors_zeropoint_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) $(LIBSIMULATE) $(LIBFSTEST)

fors_photometry_test_SOURCES = fors_photometry-test.c
fors_photometry_test_LDFLAGS = $(CPL_LDFLAGS)
fors_photometry_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) $(LIBSIMULATE) $(LIBFSTEST)

fors_img_science_test_SOURCES = fors_img_science-test.c
fors_img_science_test_LDFLAGS = $(CPL_LDFLAGS)
fors_img_science_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) $(LIBSIMULATE) $(LIBFSTEST)

fors_dfs_idp_test_SOURCES = fors_dfs_idp-test.c
fors_dfs_idp_test_LDFLAGS = $(CPL_LDFLAGS)
fors_dfs_idp_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) $(LIBSIMULATE) $(LIBFSTEST)

fors_trimm_non_illum_test_SOURCES = fors_trimm_non_illum-test.cc
fors_trimm_non_illum_test_LDFLAGS = $(CPL_LDFLAGS)
fors_trimm_non_illum_test_LDADD = $(LIBTEST) $(LIBCPLCORE) $(LIBCPLUI) $(LIBMOSCA)  $(LIBFORS) $(LIBSIMULATE) $(LIBFSTEST)


#TESTS          = $(check_PROGRAMS) $(check_SCRIPTS)
TESTS          = $(check_PROGRAMS)
#TESTS          = fors_photometry-test

# Expected failures
# XFAIL_TESTS =

# Be sure to reexport important environment variables.
TESTS_ENVIRONMENT = MAKE="$(MAKE)" CC="$(CC)" CFLAGS="$(CFLAGS)" \
        CPPFLAGS="$(CPPFLAGS)" LD="$(LD)" LDFLAGS="$(LDFLAGS)" \
        LIBS="$(LIBS)" LN_S="$(LN_S)" NM="$(NM)" RANLIB="$(RANLIB)" \
        OBJEXT="$(OBJEXT)" EXEEXT="$(EXEEXT)" MALLOC_PERTURB_=231 \
        MALLOC_CHECK_=2


# We need to remove any files that the above tests created.
clean-local:
	 $(RM) *.fits *.paf *.log
