/* $Id: fors_trimm_illum-test.c,v 1.0 2017-11-20 18:45:53 msalmist Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: msalmist $
 * $Date: 2017-11-20 18:45:53 $
 * $Revision: 1.0 $
 * $Name: not supported by cvs2svn $
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "test.h"
#include "fs_test.h"
#include "fors_dfs.h"
#include "test_simulate.h"
#include "fors_trimm_illum.h"

/**
 * @defgroup fors_trim_illum - Tests for the fors_trimm_illum module
 */

/**@{*/

void test_trimm_non_illum(void);

void test_trimm_non_illum(void){
    cpl_propertylist * wcs = cpl_propertylist_new();
    fors_setting * setting = NULL;
    fors_image * image = NULL;
    cpl_table *illum_table = NULL;
    bool success;

    //Check that null entries return false
    success = fors_trimm_non_illum(image, wcs, setting, illum_table);
    cpl_test_eq(success, false);


    /* Simulate data */
    const char* sci_name = "science.fits";
    auto frame =  create_standard(sci_name, SCIENCE_IMG, CPL_FRAME_GROUP_RAW);
    image = fors_image_load(frame);
    setting = fors_setting_new(frame);

    const char* illum_name = "illum.fits";
    auto frame2 =  create_illum_table(illum_name, DETECTOR_ILLUMINATED_REGION, 
                                      CPL_FRAME_GROUP_RAW);
    illum_table = cpl_table_load(cpl_frame_get_filename(frame2), 1, 1);

    //Actually trimming the image
    fors_dfs_copy_wcs(wcs, frame);
    success = fors_trimm_non_illum(image, wcs, setting, illum_table);

    //The illuminated region created by create_illum_table is
    //(11, 1) - (110, 150) and binning is 2
    cpl_test_eq(fors_image_get_size_x(image), 50);
    cpl_test_eq(fors_image_get_size_y(image), 75);
    cpl_test_eq(cpl_propertylist_get_double(wcs, "CRPIX1"), -4);
    cpl_test_eq(success, true);

    //Cleanup
    delete_test_file(sci_name);\
    fors_image_delete(&image);
    fors_setting_delete(&setting);
    cpl_table_delete(illum_table);
    cpl_propertylist_delete(wcs);
    cpl_frame_delete(frame);
    cpl_frame_delete(frame2);

    cpl_test_eq(cpl_error_get_code(), CPL_ERROR_NONE);
}

/**
 * @brief   Test of idp module
 */
int main(void)
{
    TEST_INIT;

    test_trimm_non_illum();

    TEST_END;
}

/**@}*/
