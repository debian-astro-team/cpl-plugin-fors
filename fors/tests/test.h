/*                                                                              *
 *   This file is part of the ESO IRPLIB package                                *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
#ifndef TEST_H
#define TEST_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
  @brief Initialize CPL + CPL messaging + IRPLIB + unit test
  @note  This macro should be used at the beginning of main() 
 */
/*----------------------------------------------------------------------------*/
#define TEST_INIT cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING)

/*----------------------------------------------------------------------------*/
/**
  @brief  End unit test
  @note   This macro should be used at the end of main() 
 */
/*----------------------------------------------------------------------------*/
#define TEST_END return cpl_test_end(0)


/*-----------------------------------------------------------------------------
                               Functions prototypes
 -----------------------------------------------------------------------------*/
void 
test_recipe_output(const cpl_frameset *frames,
                   const char *const product_tags[], int n_prod,
		   const char *main_product,
		   const char *const qc[], int n_qc);

/**@}*/

#endif
