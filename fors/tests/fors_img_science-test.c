/* $Id: fors_img_science-test.c,v 1.6 2011-07-19 15:50:16 cgarcia Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2011-07-19 15:50:16 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_img_science_impl.h>
#include <fors_dfs.h>
#include <fors_utils.h>

#include <fs_test.h>

#include <test_simulate.h>
#include <test.h>

/**
 * @defgroup fors_img_science_test  science recipe tests
 */

/**@{*/

#undef cleanup
#define cleanup \
do { \
    cpl_frameset_delete(frames); \
    cpl_parameterlist_delete(parameters); \
    delete_test_file(img_std_name);\
    delete_test_file(img_master_name);\
    delete_test_file(img_sky_name);\
    delete_test_file(img_cat_name);\
    delete_test_file(tab_name);\
    delete_test_files_from_tag(product_tags);\
} while(0)

/**
 * @brief  Test science recipe
 */
static void
test_img_science(void)
{
    /* Input */
    cpl_frameset      *frames     = cpl_frameset_new();
    cpl_parameterlist *parameters = cpl_parameterlist_new();
    cpl_parameter     *p          = NULL;
    double exptime = 1.0;

    const char* img_std_name = "img_science_standard_img.fits";
    const char* img_master_name = "img_science_master_bias.fits";
    const char* img_sky_name = "img_science_master_sky_flat.fits";
    const char* img_cat_name = "img_science_std_cat.fits";
    const char* tab_name = "img_science_phot_table.fits";

    /* Test existence of QC + products */
    const char *product_tags[] = {SOURCES_SCI,
                                        SCIENCE_REDUCED_IMG,
                                        PHOTOMETRY_TABLE,
                                        PHOT_BACKGROUND_SCI_IMG};

    /* Products */
    
    /* Simulate data */
    cpl_frameset_insert(frames, create_standard(img_std_name,
                                                SCIENCE_IMG,
                                                CPL_FRAME_GROUP_RAW));
    cpl_frameset_insert(frames, create_master_bias(img_master_name,
                                            MASTER_BIAS,
                                            CPL_FRAME_GROUP_CALIB));
    cpl_frameset_insert(frames, create_master_sky_flat(img_sky_name,
                                                MASTER_SKY_FLAT_IMG,
                                                CPL_FRAME_GROUP_CALIB, exptime));
    cpl_frameset_insert(frames, create_std_cat(img_cat_name,
                                               FLX_STD_IMG,
                                               CPL_FRAME_GROUP_CALIB));
    cpl_frameset_insert(frames, create_phot_table(tab_name,
                                                  PHOT_TABLE,
                                                  CPL_FRAME_GROUP_CALIB));
    
    /* Set parameters */
    fors_img_science_define_parameters(parameters);
    p = cpl_parameter_new_enum("fors.fors_img_science.extract_method",
                               CPL_TYPE_STRING,
                               "Source extraction method",
                               "fors.fors_img_science",
                               "sex", 2,
                               "sex", "test");
    cpl_parameterlist_append(parameters, p);
    assure( !cpl_error_get_code(), return, 
            "Create parameters failed");
    
    fors_parameterlist_set_defaults(parameters);
 
    /* Do not rely on SExtractor for this unit test */
    cpl_parameter_set_string(cpl_parameterlist_find(parameters,
                                                    "fors.fors_img_science.extract_method"),
                             "test");

    fors_img_science(frames, parameters);

    const char *main_product = SCIENCE_REDUCED_IMG;

    const char *const qc[] = 
        {"QC SKYAVG", "QC SKYMED", "QC SKYRMS",
         "QC IMGQU", "QC IMGQUERR", "QC STELLAVG", 
         "QC IMGQUELL", "QC IMGQUELLERR"};
    test_recipe_output(frames, 
                       product_tags, sizeof product_tags / sizeof *product_tags,
		       main_product,
                       qc, sizeof qc / sizeof *qc);
        
    cleanup;
    return;
}

#undef cleanup
#define cleanup \
do { \
    cpl_frameset_delete(frames); \
    cpl_parameterlist_delete(parameters); \
    delete_test_file(img_std_name);\
    delete_test_file(img_master_name);\
    delete_test_file(img_sky_name);\
    delete_test_file(img_cat_name);\
    delete_test_file(tab_coeff_name);\
    delete_test_file(tab_aligned_phot_name);\
    delete_test_files_from_tag(product_tags);\
} while(0)

/**
 * @brief   Test of img_science recipe
 */
int main(void)
{
    TEST_INIT;

    /* cpl_msg_set_level(CPL_MSG_DEBUG); */
    test_img_science();


    TEST_END;
}

/**@}*/
