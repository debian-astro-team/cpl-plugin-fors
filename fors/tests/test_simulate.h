/* $Id: test_simulate.h,v 1.9 2007-10-18 12:48:12 jmlarsen Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: jmlarsen $
 * $Date: 2007-10-18 12:48:12 $
 * $Revision: 1.9 $
 * $Name: not supported by cvs2svn $
 */

#ifndef TEST_SIMULATE_H
#define TEST_SIMULATE_H

#include <fors_image.h>

#include <cpl.h>

CPL_BEGIN_DECLS

cpl_frame *
create_bias(const char *filename, const char *tag, cpl_frame_group group);

cpl_frame *
create_master_bias(const char *filename, const char *tag, cpl_frame_group group);

cpl_frame *
create_screen_flat(const char *filename, const char *tag, cpl_frame_group group);

cpl_frame *
create_sky_flat(const char *filename, const char *tag, cpl_frame_group group,
                double exptime);

cpl_frame *
create_master_sky_flat(const char *filename, const char *tag, 
                       cpl_frame_group group, double exptime);

cpl_frame *
create_standard(const char *filename, const char *tag, cpl_frame_group group);

cpl_frame *
create_dark(const char *filename, const char *tag, cpl_frame_group group, const int number_saturated);

cpl_frame *
create_std_cat(const char *filename, const char *tag, cpl_frame_group group);

cpl_frame *
create_phot_table(const char *filename, const char *tag, cpl_frame_group group);

cpl_frame *
create_zeropint_header_table(const char *filename, const char *tag, cpl_frame_group group,
                             const double zp, const double zp_err);

cpl_frame *
create_illum_table(const char *filename, const char *tag, cpl_frame_group group);

void
create_standard_keys(cpl_propertylist *header, double exptime);

CPL_END_DECLS

#endif
