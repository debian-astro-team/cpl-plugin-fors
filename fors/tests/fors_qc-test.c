/* $Id: fors_qc-test.c,v 1.8 2007-10-12 11:17:47 jmlarsen Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: jmlarsen $
 * $Date: 2007-10-12 11:17:47 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_qc.h>
#include <test.h>

/**
 * @defgroup fors_qc_test QC test
 */

/**@{*/

/**
 * @brief   Write QC log
 */
static void
test_qc(void)
{
    const char *const qcdic_version = "2.0";
    const char *const instrument[] = {"FORS1", "FORS2"};
    const char *const qc_name[] = {"SOME.QC", "SOME.OTHER.QC"};
    const char *const qc_value[] = {"some value", "some other value"};
    const char *const qc_comment[] = {"some comment", "some other comment"};
    const char *const keyword_name[] = {"ESO SOME KEYWORD", 
                                        "ESO SOME OTHER KEYWORD"};
    const char *const keyword_value[] = {"some key value", 
                                         "some other key value"};
    const char *const keyword_unit[] = {"some key unit", 
                                         "some other key unit"};
    const char *const keyword_comment[] = {"some key comment", 
                                           "some other key comment"};
    
    
    cpl_propertylist *header = cpl_propertylist_new();
    int i;
    
    for (i = 0; i < 2; i++) {
        cpl_propertylist_append_string(header, 
                                       keyword_name[i],
                                       keyword_value[i]);
    }
    
    for (i = 0; i < 2; i++) {
        fors_qc_start_group(header, qcdic_version, 
                            instrument[i]);
        
        fors_qc_write_string(qc_name[i], qc_value[i],
                             qc_comment[i], instrument[i]);
        
        fors_qc_write_double("QC.ZEROPOINT", 5.3, "mag",
                             "mean zeropoint", instrument[i]);
        
        fors_qc_keyword_to_paf(header, 
                               keyword_name[i],
                               keyword_unit[i],
                               keyword_comment[i],
                               instrument[i]);
        
        fors_qc_end_group();
    }

    cpl_propertylist_delete(header);
    return;
}


/**
 * @brief   Test of QC module
 */
int main(void)
{
    TEST_INIT;

    test_qc();

    TEST_END;
}

/**@}*/
