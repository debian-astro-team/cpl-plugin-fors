/* $Id: fors_setting-test.c,v 1.2 2007-09-27 12:48:52 jmlarsen Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: jmlarsen $
 * $Date: 2007-09-27 12:48:52 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_setting.h>

#include <fors_dfs.h>

#include <test_simulate.h>
#include <test.h>

#include <fs_test.h>


#include <cpl.h>

/**
 * @defgroup fors_setting_test  Instrument setting
 */

/**@{*/

#undef cleanup
#define cleanup \
do { \
    fors_setting_delete(&setting); \
    cpl_frame_delete(sflat); \
    delete_test_file(fname);\
} while (0)
/**
 * @brief   Test instrument setting
 */
static void
test_setting(void)
{
    fors_setting *setting = NULL;
    double exptime = 1.0;
    const char* fname = "setting_raw.fits";
    cpl_frame *sflat = create_sky_flat(fname,
                                       SKY_FLAT_IMG, CPL_FRAME_GROUP_RAW,
                                       exptime);

    setting = fors_setting_new(sflat);

    cpl_test_rel(setting->exposure_time, exptime, 0.001);
   
    cleanup;
    return;
}

/**
 * @brief   Test of image setting module
 */
int main(void)
{
    TEST_INIT;

    /* cpl_msg_set_level(CPL_MSG_DEBUG); */
    test_setting();

    TEST_END;
}

/**@}*/
