/* $Id: fors_dfs_idp.h,v 1.4 2010-09-14 07:49:30 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: msalmist $
 * $Date: 2017-11-23 10:32:05 $
 * $Revision: 1.0 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_DFS_IDP_H
#define FORS_DFS_IDP_H

#include <cpl.h>

#include "fors_image.h"

CPL_BEGIN_DECLS

/**
 * @defgroup IDP header generator with corresponding structs
*/

/**@{*/

/*Struct codifying the relationship between two keys*/
typedef struct {
	char * source_key; /*Owning*/
	char * dest_key; /*Owning*/
	char * dest_comment; /*Owning*/
	cpl_property * override_prop; /*Property to override*/
} fors_dfs_idp_property_converter;

fors_dfs_idp_property_converter * fors_dfs_idp_property_converter_new(const char * source_key,
		const char * dest_key, const char * dest_comment, const cpl_propertylist * override_list);

void fors_dfs_idp_property_converter_delete(fors_dfs_idp_property_converter *  conv);

cpl_error_code fors_dfs_idp_property_converter_convert(const fors_dfs_idp_property_converter * self,
		const cpl_propertylist * product_list, cpl_propertylist * append_to_list);

/*Struct codifying a list of fors_dfs_idp_property_converter*/
typedef struct {

	fors_dfs_idp_property_converter ** converters;
	cpl_size num_converters;

}fors_dfs_idp_property_converter_list;

fors_dfs_idp_property_converter_list *
fors_dfs_idp_property_converter_list_new(void);
void
fors_dfs_idp_property_converter_list_delete(fors_dfs_idp_property_converter_list * list);

cpl_error_code
fors_dfs_idp_property_converter_list_push_back(fors_dfs_idp_property_converter_list * list,
		fors_dfs_idp_property_converter * conv);

cpl_size
fors_dfs_idp_property_converter_list_get_size(const fors_dfs_idp_property_converter_list * list);

const fors_dfs_idp_property_converter *
fors_dfs_idp_property_converter_list_get_const(const fors_dfs_idp_property_converter_list * list,
		const cpl_size idx);

typedef struct{
	fors_dfs_idp_property_converter_list * converters;
	cpl_propertylist * defaults_values;
	cpl_boolean append_mjd_end;
}fors_dfs_idp_converter;

fors_dfs_idp_converter * fors_dfs_idp_converter_new(const cpl_boolean append_mjd_end);

void
fors_dfs_idp_converter_delete(fors_dfs_idp_converter * idp_conv);

cpl_error_code fors_dfs_idp_converter_add_conversion(
		fors_dfs_idp_converter * self,
		const char * source_key, const char * dest_key,
		const char * dest_comment,
		const cpl_propertylist * override_list);

cpl_error_code fors_dfs_idp_converter_add_int_default(fors_dfs_idp_converter * self,
		const char * name, const char * comment, const int el);

cpl_error_code fors_dfs_idp_converter_add_real_default(fors_dfs_idp_converter * self,
		const char * name, const char * comment, const double el);

cpl_error_code fors_dfs_idp_converter_add_string_default(fors_dfs_idp_converter * self,
		const char * name, const char * comment, const char * el);

cpl_error_code fors_dfs_idp_converter_add_boolean_default(fors_dfs_idp_converter * self,
		const char * name, const char * comment, const cpl_boolean el);
cpl_propertylist *
fors_dfs_idp_converter_generate_idp_propertylist(const fors_dfs_idp_converter * self,
					const cpl_propertylist * prod_plist);


typedef struct{
	double zp;
	double zp_e;
	double extinction;
}fors_idp_zp_data;

fors_dfs_idp_converter *
fors_generate_imaging_idp_converter(const cpl_propertylist * bias_plist,
		const fors_idp_zp_data * phot_data,
		const fors_image * img, const double crder,
		const double ra, const double dec,
		const double skysqdeg,
		const double psf_fwhm, const double elliptic,
		const double ab_mag_lim, const double ab_mag_sat,
                const double exptime);


CPL_END_DECLS

#endif
