#include <fors_molecfit_utils.h>


/* Is it always only one input spectrum per frameset?*/
cpl_error_code fors_molecfit_utils_find_input_frame(cpl_frameset *frameset,cpl_parameterlist* iframe){
    /* The cpl_parameterlist* of results - iframe - should already have a size of 3 allocated*/

    cpl_frameset_dump(frameset,stdout);

    /* FORS has 3 distinct observation modes */
    cpl_size n_omodes = 3;
    cpl_array* omodes = cpl_array_new(n_omodes,CPL_TYPE_STRING);
    cpl_array_set_string(omodes,0,"LSS");
    cpl_array_set_string(omodes,1,"MOS");
    cpl_array_set_string(omodes,2,"MXU");

    const char* base = "REDUCED_IDP_SCI";

    for (int i=0;i<n_omodes;i++){
        const char* omode = cpl_array_get_string(omodes,i);
        const char* tag = cpl_sprintf("%s_%s",base,omode);
        cpl_msg_info(cpl_func,"Looking for TAG [%s]",tag);
        cpl_frame* input_frame = cpl_frameset_find(frameset, tag);
        if(input_frame){
            const char* fname =  cpl_frame_get_filename(input_frame);
            cpl_parameterlist_append(iframe,cpl_parameter_new_value("INPUTNAME",CPL_TYPE_STRING,NULL,NULL,tag));
            cpl_parameterlist_append(iframe,cpl_parameter_new_value("OBSMODE",CPL_TYPE_STRING,NULL,NULL,omode));
            cpl_parameterlist_append(iframe,cpl_parameter_new_value("IDP",CPL_TYPE_STRING,NULL,NULL,"TRUE"));
            cpl_parameterlist_append(iframe,cpl_parameter_new_value("INPUTFILENAME",CPL_TYPE_STRING,NULL,NULL,fname));
            cpl_array_delete(omodes);
            return CPL_ERROR_NONE;
        }
    }
    cpl_array_delete(omodes);
    return CPL_ERROR_NULL_INPUT;
}


void fors_parameters_new_int( cpl_parameterlist* list, const char* recipe_id, const char* name,int value, const char* comment)
{
  const char* paramname;
  const char* recipename;
  cpl_parameter* p =NULL;


  recipename = cpl_sprintf("%s",recipe_id);
  paramname = cpl_sprintf("%s",name);
  //assure(list != NULL,CPL_ERROR_NULL_INPUT,"parameters list is NULL"); 

  p = cpl_parameter_new_value(paramname,CPL_TYPE_INT,comment,
    recipename,value);
  cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,name);
  cpl_parameterlist_append(list,p);

  cleanup:
    return;
}


void fors_parameters_new_boolean( cpl_parameterlist* list, const char* recipe_id, const char* name,int value, const char* comment)
{
  const char* paramname;
  const char* recipename;
  cpl_parameter* p =NULL;
  recipename = cpl_sprintf("%s",recipe_id);
  paramname = cpl_sprintf("%s",name);
  //XSH_ASSURE_NOT_NULL(list);                                                                                                                                                             
  p = cpl_parameter_new_value(paramname,CPL_TYPE_BOOL,comment,
    recipename,value);
  cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,name);
  cpl_parameterlist_append(list,p);
                                                                                                                                                             
  cleanup:
    return;
}

void fors_parameters_new_string( cpl_parameterlist* list,
				       const char* recipe_id,
				       const char* name,
				       const char* value,
				       const char* comment)
{

  const char* paramname = NULL ;
  const char* recipename = NULL ;
  cpl_parameter* p =NULL;

  recipename = cpl_sprintf("%s",recipe_id);
  paramname = cpl_sprintf("%s",name);

  //assure(list != NULL,CPL_ERROR_NULL_INPUT,"parameters list is NULL"); 

  p = cpl_parameter_new_value(paramname,CPL_TYPE_STRING, comment,
				    recipename, value);
  cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,name);
  cpl_parameterlist_append(list,p);

 cleanup:
  //XSH_FREE(recipename);
  //XSH_FREE(paramname);
  return;
}

void fors_parameters_new_double( cpl_parameterlist* list,
  const char* recipe_id,const char* name,double value, const char* comment)
{
  const char* paramname;
  const char* recipename;
  cpl_parameter* p =NULL;

  recipename = cpl_sprintf("%s",recipe_id);
  paramname = cpl_sprintf("%s",name);

  //assure(list != NULL,CPL_ERROR_NULL_INPUT,"parameters list is NULL");

  p = cpl_parameter_new_value(paramname,CPL_TYPE_DOUBLE,comment,
    recipename,value);
  cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,name);
  cpl_parameterlist_append(list,p);

  cleanup:
    return;
}


void fors_parameters_new_float( cpl_parameterlist* list, const char* recipe_id,const char* name,float value, const char* comment)
{
  const char* paramname;
  const char* recipename;
  cpl_parameter* p =NULL;


  recipename = cpl_sprintf("%s",recipe_id);
  paramname = cpl_sprintf("%s",name);
  //assure(list != NULL,CPL_ERROR_NULL_INPUT,"parameters list is NULL");

  p = cpl_parameter_new_value(paramname,CPL_TYPE_FLOAT,comment,
    recipename,value);
  cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,name);
  cpl_parameterlist_append(list,p);

  cleanup:
    return;
}

/*----------------------------------------------------------------------------*/
/**
 * @brief    Checks the validity of the range wavlength coverage and that of the associated extension data
 *
 * @param    rangtab       The range table.
 * @param    data          The science data molecfit_fits object.
 */
/*----------------------------------------------------------------------------*/
//REALLY SHOULD MOVE THIS TO TELLURICCORR
cpl_error_code fors_molecfit_model_check_extensions_and_ranges(cpl_size extension, double min_wav, double max_wav, cpl_table* range) {
  //cpl_msg_info(cpl_func,"fors_molecfit_model_check_extensions_and_ranges");
  //cpl_msg_info(cpl_func,"selected rows: %lld",cpl_table_count_selected(range));

  //cpl_msg_info (cpl_func,"CHECK FOR extension %lld, min %f max %f",  extension, min_wav,max_wav);
  //cpl_table_dump(range,0,cpl_table_get_nrow(range),NULL);
  
  /* Check all ranges that are mapped to this extension */
  
  cpl_size nranges=cpl_table_get_nrow(range);
  cpl_boolean check_ok= CPL_TRUE;
  for (cpl_size i=0;i<nranges;i++) {
      
      cpl_size range_idx=i+1;
     
      int    range_assigned_extension =  cpl_table_get_int   (range,MF_COL_WAVE_RANGE_MAP2CHIP,i,NULL);
      double range_low_limit          =  cpl_table_get_double(range,MF_COL_WAVE_RANGE_LOWER,        i, NULL);
      double range_upp_limit          =  cpl_table_get_double(range,MF_COL_WAVE_RANGE_UPPER,        i,NULL);
      
      /* Only check ranges that have been mapped to this extension data */
      if (range_assigned_extension!=extension) continue;

      if(range_upp_limit<=min_wav || range_low_limit>=max_wav){
        cpl_table_unselect_row(range,i);
      }
  }

  cpl_msg_info(cpl_func,"selected rows: %lld",cpl_table_count_selected(range));
  if(cpl_table_count_selected(range) == 0){
      check_ok = CPL_FALSE;
  }
     
      /* If the check is not ok then set error message and return illegal input flag */
      if (!check_ok) {
         cpl_msg_error(cpl_func,"No valid ranges");
         //cpl_msg_error(cpl_func,"Range %lld [%f , %f] Assigned to Extension %d [%f , %f]. Check Overlap NOT OK",
         //                    range_idx, range_low_limit, range_upp_limit, range_assigned_extension,min_wav,max_wav);
         return CPL_ERROR_ILLEGAL_INPUT;
      }
      
      /* Output that this range has been checked and show details */ 
      //cpl_msg_info(cpl_func,"Range %lld [%f , %f] Assigned to Extension %d [%f , %f]. Check Overlap OK", 
      //                         range_idx, range_low_limit, range_upp_limit, range_assigned_extension,min_wav,max_wav);

  

  return CPL_ERROR_NONE;
}
