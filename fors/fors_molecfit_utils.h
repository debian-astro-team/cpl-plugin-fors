/*
 * This file is part of the ESO X-Shooter Pipeline
 * Copyright (C) 2001-2018 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef FORS_MOLECFIT_UTILS_H
#define FORS_MOLECFIT_UTILS_H
/*----------------------------------------------------------------------------*/
/**
 *                              Includes
 */
/*----------------------------------------------------------------------------*/

/* Include both telluriccorr *and* our extra wrapper codes, since
   we deliberately don't want to have them (pre-included) in telluriccorr.h
   to ensure telluriccorr is still comptabile with molecfit_model
*/

#include <string.h>
#include <math.h>

#include <cpl.h>

#include <telluriccorr.h>

cpl_error_code fors_molecfit_utils_find_input_frame(cpl_frameset *frameset,cpl_parameterlist* iframe);
void fors_parameters_new_int(cpl_parameterlist* list, const char* recipe_id, const char* name,int value, const char* comment);
void fors_parameters_new_boolean(cpl_parameterlist* list, const char* recipe_id, const char* name,int value, const char* comment);
void fors_parameters_new_string(cpl_parameterlist* list, const char* recipe_id, const char* name, const char* value, const char* comment);
void fors_parameters_new_double(cpl_parameterlist* list, const char* recipe_id,const char* name,double value, const char* comment);
void fors_parameters_new_float(cpl_parameterlist* list, const char* recipe_id,const char* name,float value, const char* comment);

cpl_error_code fors_molecfit_model_check_extensions_and_ranges(cpl_size extension, double min_wav, double max_wav, cpl_table* range);


#endif /*FORS_MOLECFIT_MODEL_H*/
