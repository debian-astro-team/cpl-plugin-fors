/*
 * This file is part of the FORS Data Reduction Pipeline
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdexcept>
#include "cpl.h"
#include "fors_detmodel.h"
#include <math.h>


//The detector model is the usual for CCDs: a readout noise component
//and a photon noise component scaled by the gain. 
//The readout noise is taken from the overscan region of each readout port
//The gain is taken as the nominal gain from the detector.
//If ccd_config is created with fiera_config(header), the nominal gain
//is taken from the header as ESO DET OUT? GAIN, for instance, also per 
//readout port.
void fors_image_variance_from_detmodel(fors_image * image,
                                       const mosca::ccd_config& ccd_config,
                                       std::vector<double>& overscan_level,
									   double * average_overscan_level)
{
    //Check the number of overscan level is consistent with number of ports
    if(ccd_config.nports() != overscan_level.size())
        throw std::invalid_argument("Mismatch between overscan levels and ports");
    
    
    //Images which contain the model parameters per port
    cpl_size im_nx = fors_image_get_size_x(image);
    cpl_size im_ny = fors_image_get_size_y(image);
    cpl_image * ron_sq_im  = cpl_image_new(im_nx, im_ny, CPL_TYPE_FLOAT);
    cpl_image * gain_im = cpl_image_new(im_nx, im_ny, CPL_TYPE_FLOAT);
    cpl_image * os_im   = cpl_image_new(im_nx, im_ny, CPL_TYPE_FLOAT);
    
    //Loop on each port:
    for(size_t iport = 0; iport < ccd_config.nports(); iport++)
    {
        double ron  = ccd_config.computed_ron(iport);
        
        double gain = ccd_config.nominal_gain(iport);
        
        mosca::rect_region port_reg = 
                        ccd_config.port_region(iport).coord_0to1();

        //TODO: Use cpl_image_fill_window when it exists.
        for(cpl_size ix = port_reg.llx(); ix <= port_reg.urx(); ++ix)
            for(cpl_size iy = port_reg.lly(); iy <= port_reg.ury(); ++iy)
            {
                cpl_image_set(ron_sq_im, ix, iy, ron * ron);
                cpl_image_set(gain_im, ix, iy, gain);
                cpl_image_set(os_im, ix, iy, overscan_level[iport]);
            }
    }
    
    cpl_image * ima_sub_os = cpl_image_subtract_create(image->data, os_im);

    if(average_overscan_level){
    	*average_overscan_level = cpl_image_get_mean(os_im);
    }

    //Negative values would give less variance, which doesn't make sense
    //They appear because the values are distributed with a convolution
    //of poisson+gaussian distribution or because the overscan has too larger
    //values. We set all the negative values to 0.
    cpl_mask * neg_mask = cpl_mask_threshold_image_create(ima_sub_os, 0., 
                                                cpl_image_get_max(ima_sub_os));
    cpl_mask_not(neg_mask);
    cpl_image_reject_from_mask(ima_sub_os, neg_mask);
    cpl_image_fill_rejected(ima_sub_os, 0);
    cpl_image_accept_all(ima_sub_os);

    /* flux_photoel = flux_adu / gain
     * poisson_variance_photel = flux_photoel  -> Poisson law
     * poisson_variance_adu    = poisson_variance_photel * gain * gain
     * poisson_variance_adu    = flux_adu * gain */
    cpl_image * poisson_var_adu = cpl_image_multiply_create(ima_sub_os, gain_im);

    cpl_image * variance = cpl_image_add_create(poisson_var_adu, ron_sq_im);
    
    cpl_image_copy(image->variance, variance, 1, 1);
    
    cpl_image_delete(ron_sq_im);
    cpl_image_delete(gain_im);
    cpl_image_delete(os_im);
    cpl_image_delete(ima_sub_os);
    cpl_image_delete(poisson_var_adu);
    cpl_image_delete(variance);
    cpl_mask_delete(neg_mask);
}

void fors_image_variance_from_detmodel(fors_image_list * ima_list,
                                       const mosca::ccd_config& ccd_config,
                                       std::vector<double>& overscan_level)
{
    int nima = fors_image_list_size(ima_list);

    fors_image * target_ima = fors_image_list_first(ima_list);
    for(int ima = 0; ima < nima; ++ima)
    {
        fors_image_variance_from_detmodel(target_ima, ccd_config, 
                                          overscan_level);
        target_ima = fors_image_list_next(ima_list);
    }    

}
