/*
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */


#ifndef FORS_SPEC_IDP_H
#define FORS_SPEC_IDP_H

#include <cpl.h>
#include "fors_image.h"
#include "fors_star.h"


typedef struct
{
    cpl_frameset * frameset;
    const cpl_parameterlist * parlist;
    const cpl_frame * iherit_frame;
    const char * recipe_name;
    const char * idp_save_tag;
    const char * reduced_science_tag;
    const char * master_bias_tag;
    const char * disp_coeff_tag;
    double spec_sye;
    double exptime;
    const char * det_exp_num;
}fors_spec_idp_save_info;

void fors_spec_idp_save_idp_format(fors_spec_idp_save_info& info, const cpl_image * calibrated,
    const cpl_image * calibrated_error, const cpl_image * bkg,
    const cpl_image * reduced, const cpl_image * reduced_err,
    const cpl_array * wlens, const cpl_table * obj_table);

double fors_spec_idp_get_spec_sye(const cpl_table * offsets,
    const cpl_table * slits);

#endif
