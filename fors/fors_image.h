/* $Id: fors_image.h,v 1.31 2013-07-24 12:59:35 cgarcia Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-07-24 12:59:35 $
 * $Revision: 1.31 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_IMAGE_H
#define FORS_IMAGE_H

#include <fors_setting.h>
#include <cpl.h>
#include <hdrl.h>

typedef struct _fors_image fors_image;

extern const cpl_type FORS_IMAGE_TYPE;

/* Container */
#undef LIST_ELEM
#define LIST_ELEM fors_image
#include <list.h>

CPL_BEGIN_DECLS

/**
 * @defgroup fors_image Image with error bars
 *
 * This object contains 
 * - the image data values,
 * - the image data error bars,
 * - additional relevant properties for the image 
 *   (detector overscan areas, exposure time, etc.)
 *
 * For efficiency+convenience the error bars are internally represented
 * as sigma^2 to avoid the repeated conversion between sigma <-> variance
 */

/**@{*/

struct _fors_image
{
    cpl_image *data;
    cpl_image *variance;

    /* Invariants:
       The CPL images are non-NULL. 
       The variance image is everywhere non-negative.
       The CPL image types are FORS_IMAGE_TYPE.
       The CPL image bad pixel masks are unused
    */
};

/* Constructors */
fors_image *fors_image_new(cpl_image *data, cpl_image *weights);

fors_image *fors_image_duplicate(const fors_image *image);

/* Desctructors */
void fors_image_delete(fors_image **image);
void fors_image_delete_const(const fors_image **image);


/* I/O */
fors_image *fors_image_load(const cpl_frame *frame);


fors_image_list *fors_image_load_list(const cpl_frameset *frames);

const fors_image_list *
fors_image_load_list_const(const cpl_frameset *frames);

void
fors_image_save(const fors_image *image, const cpl_propertylist *header,
                const cpl_propertylist *err_header, const char *filename);

void
fors_image_save_sex(const fors_image *image, const cpl_propertylist *header,
                    const char *filename_dat,
                    const char *filename_var,
                    int radius);

/* Other */
cpl_size fors_image_get_size_x(const fors_image *image);
cpl_size fors_image_get_size_y(const fors_image *image);
const float *fors_image_get_data_const(const fors_image *image);

void fors_image_draw(fors_image *image, int type,
                     double x, double y,
                     int radius, double color);

void fors_image_crop(fors_image *image,
                     int xlo, int ylo,
                     int xhi, int yhi);

/* Arithmetic */
void fors_image_subtract(fors_image *left, const fors_image *right);
void fors_image_multiply(fors_image *left, const fors_image *right);
void fors_image_multiply_noerr(fors_image *left, const cpl_image *right);
void fors_image_divide(fors_image *left, const fors_image *right);
void fors_image_divide_noerr(fors_image *left, cpl_image *right);
void fors_image_abs(fors_image *image);
void fors_image_square(fors_image *image);

void fors_image_exponential(fors_image *image, double b, double db);
void fors_image_multiply_scalar(fors_image *image, double s, double ds);
void fors_image_divide_scalar(fors_image *image, double s, double ds);
void fors_image_subtract_scalar(fors_image *image, double s, double ds);

fors_image *fors_image_collapse_create(const fors_image_list *images);
fors_image *fors_image_collapse_median_create(const fors_image_list *images);
fors_image *fors_image_collapse_minmax_create(const fors_image_list *images, 
                                              int low, int high);
fors_image *fors_image_collapse_ksigma_create(const fors_image_list *images, 
                                              int low, int high, int iter);

cpl_image *
fors_image_filter_median_create(const fors_image *image, 
                                int xradius,
                                int yradius,
                                int xstart, 
                                int ystart,
                                int xend,
                                int yend,
                                int xstep,
                                int ystep,
                                bool use_data);

cpl_image *
fors_image_flat_fit_create(fors_image *image,
                           int step,
                           int degree,
                           float level);
cpl_image *
fors_image_filter_max_create(const fors_image *image,
                             int xradius,
                             int yradius,
                             bool use_data);

/* Statistics */
double fors_image_get_mean(const fors_image *image, double *dmean);
double fors_image_get_median(const fors_image *image, double *dmedian);

double fors_image_get_stdev(const fors_image *image, double *dstdev);
double fors_image_get_stdev_robust(const fors_image *image, 
                                   double cut,
                                   double *dstdev);
double fors_image_get_error_mean(const fors_image *image, double *dmean);

double fors_image_get_min(const fors_image *image);
double fors_image_get_max(const fors_image *image);

hdrl_imagelist * fors_image_list_to_hdrl(const fors_image_list * imalist);

fors_image * fors_image_from_hdrl(const hdrl_image * image);

CPL_END_DECLS

#endif
