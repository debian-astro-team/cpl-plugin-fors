/* $Id: fors_std_star.h,v 1.14 2010-09-14 07:49:30 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2010-09-14 07:49:30 $
 * $Revision: 1.14 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_STD_STAR_H
#define FORS_STD_STAR_H

#include <fors_point.h>

#include <cpl.h>

#include <stdbool.h>

CPL_BEGIN_DECLS

typedef struct _fors_std_star 
{
    fors_point  *pixel;
    double      ra, dec;
    double      magnitude;      /* Color corrected according to filter used */
    double      dmagnitude;     /* 1 sigma error */
    double      cat_magnitude;  /* From catalog */
    double      dcat_magnitude; /* From catalog */
    double      color;          /* color, defined as difference between bands */
    double      dcolor;         /* 1 sigma color error */
    double      cov_catm_color; /* covariance(cat_magnitude, color) */
    const char  *name;
    bool        trusted;
} fors_std_star;

#undef LIST_ELEM
#define LIST_ELEM fors_std_star
#include <list.h>

fors_std_star   *fors_std_star_new(         double ra, double dec,
                                            double m, double dm,
                                            double cat_m, double dcat_m,
                                            double col, double dcol,
                                            double cov_catm_col,
                                            const char *name);

fors_std_star   *fors_std_star_new_from_table(
                                            const cpl_table *tab,
                                            unsigned int    row,
                                            const char      *ra_col,
                                            const char      *dec_col,
                                            const char      *mag_col,
                                            const char      *dmag_col,
                                            const char      *catmag_col,
                                            const char      *dcatmag_col,
                                            const char      *color_col,
                                            const char      *dcolor_col,
                                            const char      *cov_catm_color_col,
                                            const char      *x_col,
                                            const char      *y_col,
                                            const char      *name_col);

void            fors_std_star_delete(       fors_std_star **s);

void            fors_std_star_delete_const( const fors_std_star **s);

fors_std_star   *fors_std_star_duplicate(   const fors_std_star *s);

void            fors_std_star_set_name(     fors_std_star   *s,
                                            const char      *name);

bool            fors_std_star_equal(        const fors_std_star *s,
                                            const fors_std_star *t);

void            fors_std_star_print(        cpl_msg_severity level,
                                            const fors_std_star *star);

void            fors_std_star_print_list(   cpl_msg_severity level,
                                            const fors_std_star_list *sl);

bool            fors_std_star_brighter_than(const fors_std_star *s,
                                            const fors_std_star *t,
                                            void *data);

double          fors_std_star_dist_arcsec(  const fors_std_star *s,
                                            const fors_std_star *t);

void            fors_std_star_compute_corrected_mag(
                                            fors_std_star   *s,
                                            double          color_term,
                                            double          dcolor_term);

CPL_END_DECLS

#endif
