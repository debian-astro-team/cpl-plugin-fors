/* $Id: fors_instrument.h,v 1.3 2010-09-14 07:49:30 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2010-09-14 07:49:30 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_INSTRUMENT_H
#define FORS_INSTRUMENT_H

#include <fors_setting.h>
#include <stdbool.h>
#include <cpl.h>

CPL_BEGIN_DECLS

char
fors_instrument_filterband_get_by_setting(  const fors_setting  *setting);

char
fors_instrument_filterband_get_by_name(     const char  *filtername);

bool
fors_instrument_filterband_is_defined(      char    band);

bool
fors_instrument_filterband_is_none(         char    band);

bool
fors_instrument_filterband_is_unknown(      char    band);

char
fors_instrument_filterband_value_unknown(   void);

int
fors_instrument_known_filters_get_number(   void);

const char  *
fors_instrument_known_filters_get_name(     int n);

char
fors_instrument_known_filters_get_band(     int n);

CPL_END_DECLS
#endif  /* FORS_INSTRUMENT_H */
