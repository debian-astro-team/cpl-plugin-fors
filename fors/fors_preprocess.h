/* 
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */


#ifndef FORS_PREPROCESS_H
#define FORS_PREPROCESS_H

#include <fors_image.h>
#include <cpl.h>
#include <hdrl.h>


CPL_BEGIN_DECLS

/* I/O */
fors_image *fors_image_load_preprocess(const cpl_frame *frame,
                                       const cpl_frame *bias_frame);

CPL_END_DECLS

#endif
