/* $Id: fors_stats.c,v 1.00 2017-02-16 09:00:00 msalmist Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: msalmsit $
 * $Date: 2017-02-16 09:00:00 $
 * $Revision: 1.00 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_stats.h>
#include <fors_utils.h>
#include <fors_image.h>
#include <math.h>
#include <cpl.h>


/*----------------------------------------------------------------------------*/
/**
 * @defgroup fors_stats Statistical functions for header update
 */
/*----------------------------------------------------------------------------*/

/**@{*/

#undef cleanup
#define cleanup
/**
 * @brief   Writes in header the maximum value of the provided frame
 * @param   image      Image
 * @param   list       Image header
 * @param   tag        Name of the property
 * @return  nothing
 */
/*----------------------------------------------------------------------------*/
void fors_write_max_in_propertylist(fors_image *image, cpl_propertylist* list,
                                    const char* tag)
{
    assure( image != NULL, return, NULL );
    assure( list != NULL, return, NULL );
    assure( tag != NULL, return, NULL );

    double val = fors_image_get_max(image);

    cpl_propertylist_append_double(list, tag, val);
}


#undef cleanup
#define cleanup
/**
 * @brief   Writes in header the minimum value of the provided frame
 * @param   image      Image
 * @param   list       Image header
 * @param   tag        Name of the property
 * @return  nothing
 */
/*----------------------------------------------------------------------------*/
void fors_write_min_in_propertylist(fors_image *image, cpl_propertylist* list,
                                    const char* tag)
{
    assure( image != NULL, return, NULL );
    assure( list != NULL, return, NULL );
    assure( tag != NULL, return, NULL );

    double val = fors_image_get_min(image);

    cpl_propertylist_append_double(list, tag, val);
}

#undef cleanup
#define cleanup
/**
 * @brief   Writes in header the mean value of the provided frame
 * @param   image      Image
 * @param   list       Image header
 * @param   tag        Name of the property
 * @return  nothing
 */
/*----------------------------------------------------------------------------*/
void fors_write_mean_in_propertylist(fors_image *image, cpl_propertylist* list,
                                    const char* tag)
{
    assure( image != NULL, return, NULL );
    assure( list != NULL, return, NULL );
    assure( tag != NULL, return, NULL );

    double val = fors_image_get_mean(image, NULL);

    cpl_propertylist_append_double(list, tag, val);
}

#undef cleanup
#define cleanup
/**
 * @brief   Writes in header the standard deviation value of the provided frame
 * @param   image      Image
 * @param   list       Image header
 * @param   tag        Name of the property
 * @return  nothing
 */
/*----------------------------------------------------------------------------*/
void fors_write_stdev_in_propertylist(fors_image *image,
                                        cpl_propertylist* list,
                                        const char* tag)
{
    assure( image != NULL, return, NULL );
    assure( list != NULL, return, NULL );
    assure( tag != NULL, return, NULL );

    double val = fors_image_get_stdev(image, NULL);

    cpl_propertylist_append_double(list, tag, val);
}

#undef cleanup
#define cleanup
/**
 * @brief   Writes in header the median value of the provided frame
 * @param   image      Image
 * @param   list       Image header
 * @param   tag        Name of the property
 * @return  nothing
 */
/*----------------------------------------------------------------------------*/
void fors_write_median_in_propertylist(fors_image *image, cpl_propertylist *list,
                                    const char* tag)
{
    assure( image != NULL, return, NULL );
    assure( list != NULL, return, NULL );
    assure( tag != NULL, return, NULL );

    double val = fors_image_get_median(image, NULL);

    cpl_propertylist_append_double(list, tag, val);
}

#undef cleanup

static long count_saturated(const cpl_image * img){
	if (img == NULL) return 0;

	long el = 0;
	const cpl_size sz_x = cpl_image_get_size_x(img);
	const cpl_size sz_y = cpl_image_get_size_y(img);

	for(cpl_size x = 1; x <= sz_x; ++x){
		for(cpl_size y = 1; y <= sz_y; ++y){
			int rej = 0;
			const double v = cpl_image_get(img, x, y, &rej);
			if(rej || v >= 65000.0 || v < 1.0){
				el++;
			}
		}
	}
	return el;
}

#define cleanup
/**
 * @brief   Writes in header the number of bad pixels of the provided frames.
 * @param   image      List of Images
 * @param   list       Image header
 * @param   tag        Name of the property
 * @return  nothing
 */
/*----------------------------------------------------------------------------*/
void fors_write_num_bad_pixels_propertylist(fors_image_list *images,
                                            cpl_propertylist *list,
                                            const char* tag)
{
    assure( images != NULL, return, NULL );
    assure( list != NULL, return, NULL );
    assure( tag != NULL, return, NULL );

    int sz = fors_image_list_size(images);

    if(sz == 0)
        return;

    fors_image* img = fors_image_list_first(images);

    long long val = 0;
    do
    {
        cpl_image * cpl_im = img->data;
        const int d = count_saturated(cpl_im);
        val += d;
    }while((img = fors_image_list_next(images)) != NULL);

    cpl_propertylist_append_long_long(list, tag, val);
}

#undef cleanup
#define cleanup
/**
 * @brief   Writes in header the mean of the means of the images in the list.

 * @param   images     List of images
 * @param   list       Image header
 * @param   tag        Name of the property
 * @return  nothing
 */
/*----------------------------------------------------------------------------*/
void fors_write_images_mean_mean_in_propertylist(fors_image_list *images,
                                               cpl_propertylist *list,
                                               const char* tag)
{
    assure( images != NULL, return, NULL );
    assure( list != NULL, return, NULL );
    assure( tag != NULL, return, NULL );

    int sz = fors_image_list_size(images);

    if(sz == 0)
        return;

    fors_image* img = fors_image_list_first(images);

    double val = 0.0;
    do
    {
        double d = fors_image_get_mean(img, NULL);
        val += d / (double)sz;
    }while((img = fors_image_list_next(images)) != NULL);

    cpl_propertylist_append_double(list, tag, val);
}


#undef cleanup
#define cleanup
/**
 * @brief   Writes in header the median of the means of the images in the list.

 * @param   images     List of images
 * @param   list       Image header
 * @param   tag        Name of the property
 * @return  nothing
 */
/*----------------------------------------------------------------------------*/
void fors_write_images_median_mean_in_propertylist(fors_image_list *images,
                                                 cpl_propertylist *list,
                                                 const char* tag)
{
    assure( images != NULL, return, NULL );
    assure( list != NULL, return, NULL );
    assure( tag != NULL, return, NULL );

    int sz = fors_image_list_size(images);

    if(sz == 0)
        return;

    fors_image* img = fors_image_list_first(images);
    cpl_array* arr = cpl_array_new(sz, CPL_TYPE_DOUBLE);
    cpl_size i = 0;

    do
    {
        double d = fors_image_get_mean(img, NULL);
        cpl_array_set_double(arr, i++, d);
    }while((img = fors_image_list_next(images)) != NULL);

    double val = cpl_array_get_median(arr);
    cpl_array_delete(arr);

    cpl_propertylist_append_double(list, tag, val);
}


#undef cleanup
#define cleanup
/**
 * @brief   Writes in header the mean of the stdevs of the images in the list.

 * @param   images     List of images
 * @param   list       Image header
 * @param   tag        Name of the property
 * @return  nothing
 */
/*----------------------------------------------------------------------------*/
void fors_write_images_mean_stddev_in_propertylist(fors_image_list *images,
                                                cpl_propertylist *list,
                                                const char* tag)
{
    assure( images != NULL, return, NULL );
    assure( list != NULL, return, NULL );
    assure( tag != NULL, return, NULL );

    int sz = fors_image_list_size(images);

    if(sz == 0)
        return;

    fors_image* img = fors_image_list_first(images);

    double val = 0.0;
    do
    {
        double d = fors_image_get_stdev(img, NULL);
        val += d / (double)sz;
    }while((img = fors_image_list_next(images)) != NULL);


    cpl_propertylist_append_double(list, tag, val);
}
/**@}*/
