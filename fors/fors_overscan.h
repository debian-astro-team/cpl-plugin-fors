/* $Id: moses.h,v 1.41 2013/09/09 12:19:20 cgarcia Exp $
 *
 * This file is part of the VIMOS Pipeline
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013/09/09 12:19:20 $
 * $Revision: 1.41 $
 * $Name:  $
 */

#ifndef FORS_OVERSCAN_H
#define FORS_OVERSCAN_H

#include <cpl.h>
#include "fiera_config.h"
#include "fors_image.h"


fors_image * fors_subtract_prescan(const fors_image * image, 
                                   const mosca::ccd_config& ccd_config);

fors_image_list * fors_subtract_prescan(const fors_image_list * ima_list, 
                                        const mosca::ccd_config& ccd_config);

void fors_trimm_preoverscan(fors_image * image, 
                            const mosca::ccd_config& ccd_config);

void fors_trimm_preoverscan(fors_image_list * ima_list, 
                            const mosca::ccd_config& ccd_config);

void fors_trimm_preoverscan(cpl_mask *& mask, 
                            const mosca::ccd_config& ccd_config);

void fors_trimm_fill_info(cpl_propertylist * header,
                          const mosca::ccd_config& ccd_config);

void fors_trimm_preoverscan_fix_wcs(cpl_propertylist * header,
		                    const mosca::ccd_config& ccd_config);

bool fors_is_preoverscan_empty(const mosca::ccd_config& ccd_config);

bool fors_is_master_bias_preoverscan_corrected(cpl_propertylist * mbias_header);

std::vector<double> fors_get_bias_levels_from_overscan(fors_image *image, 
                                       const mosca::ccd_config& ccd_config);

std::vector<double> fors_get_bias_levels_from_mbias(fors_image *mbias, 
                                       const mosca::ccd_config& ccd_config);
#endif   /* FORS_OVERSCAN_H */
