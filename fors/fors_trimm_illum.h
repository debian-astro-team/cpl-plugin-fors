/* $Id: moses.h,v 1.41 2013/09/09 12:19:20 cgarcia Exp $
 *
 * This file is part of the VIMOS Pipeline
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013/09/09 12:19:20 $
 * $Revision: 1.41 $
 * $Name:  $
 */

#ifndef FORS_TRIM_ILLUM_H
#define FORS_TRIM_ILLUM_H

#include <cpl.h>
#include "fors_image.h"
#include "rect_region.h"


bool fors_trimm_non_illum(fors_image * image, 
                          cpl_propertylist * wcs_header,
                          fors_setting * setting,
                          const cpl_table * illum_region);

bool fors_trimm_non_illum(cpl_image ** image, 
                          cpl_propertylist * wcs_header,
                          fors_setting * setting,
                          const cpl_table * illum_region);

bool fors_trimm_non_illum_get_region(cpl_propertylist * wcs_header,
                                     fors_setting * setting,
                                     const cpl_table * illum_regions,
                                     mosca::rect_region& crop_region);


#endif   /* FORS_TRIM_ILLUM_H */
