/*
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */


#ifndef FORS_IMG_IDP_H
#define FORS_IMG_IDP_H

#include <cpl.h>
#include "fors_image.h"
#include "fors_star.h"

struct fors_img_idp_quality{

    const double psf_fwhm;
    const double psf_fwhm_pix;
	const double elliptic;
	const double sigma_sky;
	const double sky_mag;
	const double pre_overscan_level;
	const cpl_table * sources;

	fors_img_idp_quality(
            const double psf_fwhm,
            const double psf_fwhm_pix,
	        const double elliptic,
	        const double sigma_sky,
	        const double sky_mag,
	        const double pre_overscan_level,
	        const cpl_table * sources):
                psf_fwhm(psf_fwhm),
                psf_fwhm_pix(psf_fwhm_pix),
                elliptic(elliptic),
                sigma_sky(sigma_sky),
                sky_mag(sky_mag),
                pre_overscan_level(pre_overscan_level),
                sources(sources)
	        {}
};



struct fors_img_idp_tags{
	const char * idp_image_name;
	const char * idp_weigths_name;
	const char * idp_error_name;
	const char * recipe_name;

	fors_img_idp_tags(const char * idp_image_name,
	        const char * idp_weigths_name,
	        const char * idp_error_name,
	        const char * recipe_name) :
	            idp_image_name(idp_image_name),
	            idp_weigths_name(idp_weigths_name),
	            idp_error_name(idp_error_name),
	            recipe_name(recipe_name) {}
};

bool fors_img_idp_get_image_psf_fwhm(const fors_star_list *sources,
		const fors_setting * settings,
        const cpl_propertylist * sci_header,
        double * fwhm_idp, double * fwhm_pix_idp);


void fors_img_idp_save(cpl_frameset * frames, const fors_image * sci,
		cpl_propertylist * qc, const cpl_parameterlist * parameters,
		const cpl_frame * raw_science_frame, const cpl_frame * master_bias,
		const fors_image * master_flat, const double threshold_weigth,
		fors_img_idp_tags fnames, fors_img_idp_quality qty);

#endif
