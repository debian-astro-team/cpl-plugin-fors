/* $Id: fors_star.h,v 1.21 2010-09-14 07:49:30 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2010-09-14 07:49:30 $
 * $Revision: 1.21 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_STAR_H
#define FORS_STAR_H

#include <fors_std_star.h>

#include <cpl.h>

typedef struct _fors_star
{
    fors_point *pixel;
    double semi_major;
    double semi_minor;
    double fwhm;
    double stellarity_index;    /* in [0; 1] */
    double orientation;         /* radians */

    double magnitude;           /* instrumental magnitude (integrated ADU) */
    double dmagnitude;          /* 1 sigma error bar */

    double magnitude_corr;      /* magnitude corrected for gain, atmospheric
                                   extinction, exposure time */
    double dmagnitude_corr;

    double weight;              /* for the user of this module to define the
                                   meaning this */

    double flux_aper;
    double flux_max;

    const fors_std_star *id;    /* copy of identified, or NULL */

} fors_star;

#undef LIST_ELEM
#define LIST_ELEM fors_star
#include <list.h>

CPL_BEGIN_DECLS

fors_star *fors_star_new(                   double x, double y,
                                                        double fwhm,
                                            double smajor, double sminor,
                                                        double orientation,
                                                        double m, double dm,
                                            double si);

fors_star *fors_star_new_from_table(        const cpl_table *tab,
                                            unsigned int    row,
                                            const char      *x_col,
                                            const char      *y_col,
                                            const char      *fwhm_col,
                                            const char      *smaj_col,
                                            const char      *smin_col,
                                            const char      *theta_col,
                                            const char      *mag_col,
                                            const char      *dmag_col,
                                            const char      *stlndx_col,
											const char 		*flux_aper_col,
											const char 		*flux_aper_max_col);

bool fors_star_check_values(                const fors_star *star);

void fors_star_delete(fors_star **star);
void fors_star_delete_but_standard(fors_star **star);

fors_star *
fors_star_duplicate(const fors_star *star);

bool
fors_star_equal(const fors_star *s,
                const fors_star *t);

double fors_star_distsq(const fors_star *s, const fors_star *t);
double fors_star_extension(const fors_star *s, void *data);
double fors_star_stellarity(const fors_star *s, void *data);
double fors_star_ellipticity(const fors_star *s, void *data);
double fors_star_fwhm(const fors_star *s, void *data);

bool
fors_star_brighter_than(const fors_star *s1,
                        const fors_star *s2,
                        void *data);

void
fors_star_print(cpl_msg_severity level, const fors_star *s);

void
fors_star_print_list(cpl_msg_severity level, const fors_star_list *sl);

double
fors_star_get_x(const fors_star *s, void *data);

double
fors_star_get_y(const fors_star *s, void *data);

double
fors_star_get_zeropoint(const fors_star *s, void *data);

double
fors_star_get_zeropoint_err(const fors_star *s, void *data);

bool
fors_star_is_identified(const fors_star *s, void *data);

CPL_END_DECLS
#endif
