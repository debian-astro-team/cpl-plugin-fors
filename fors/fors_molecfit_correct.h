/*
 * This file is part of the ESO X-Shooter Pipeline
 * Copyright (C) 2001-2018 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef FORS_MOLECFIT_CORRECT_H
#define FORS_MOLECFIT_CORRECT_H
/*----------------------------------------------------------------------------*/
/**
 *                              Includes
 */
/*----------------------------------------------------------------------------*/

/* Include both telluriccorr *and* our extra wrapper codes, since
   we deliberately don't want to have them (pre-included) in telluriccorr.h 
   to ensure telluriccorr is still comptabile with molecfit_correct
*/

#include <string.h>
#include <math.h>

#include <cpl.h>

//#include <telluriccorr.h>
#include "mf_wrap.h"
#include "mf_wrap_config.h"
#include "mf_wrap_config_corr.h"
#include "mf_spectrum.h"
#include "fors_molecfit_utils.h"

/*----------------------------------------------------------------------------*/
/**
 *                 Typedefs: Enumeration types
 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 *                 Defines
 */
/*----------------------------------------------------------------------------*/


/*#define XSHOOTER_INPUT_DATA_FLUX_COLUMN "Flux"

//descriptions for the input parameters...
//by default grab the DESC already defined in molecfit_config or telluriccorr
//in case we want to define our own xshooter specific help
#define FORS_MOLECFIT_PARAMETER_LIST_DESC                MOLECFIT_PARAMETER_LIST_DESC
#define FORS_MOLECFIT_PARAMETER_FIT_DESC                 MOLECFIT_PARAMETER_FIT_DESC
#define FORS_MOLECFIT_PARAMETER_RELATIVE_VALUE_DESC      MOLECFIT_PARAMETER_RELATIVE_VALUE_DESC
#define FORS_MOLECFIT_PARAMETER_WAVE_RANGE_INCLUDE_DESC  MOLECFIT_PARAMETER_WAVE_RANGE_INCLUDE_DESC
#define FORS_MOLECFIT_PARAMETER_WAVE_RANGE_EXCLUDE_DESC  MOLECFIT_PARAMETER_WAVE_RANGE_EXCLUDE_DESC
#define FORS_MOLECFIT_PARAMETER_PIXEL_RANGE_EXCLUDE_DESC MOLECFIT_PARAMETER_PIXEL_RANGE_EXCLUDE_DESC
#define FORS_MF_PARAMETERS_COLUMN_LAMBDA_DESC            MF_PARAMETERS_COLUMN_LAMBDA_DESC
#define FORS_MF_PARAMETERS_COLUMN_FLUX_DESC              MF_PARAMETERS_COLUMN_FLUX_DESC
#define FORS_MF_PARAMETERS_COLUMN_DFLUX_DESC             MF_PARAMETERS_COLUMN_DFLUX_DESC
#define FORS_MF_PARAMETERS_DEFAULT_ERROR_DESC            MF_PARAMETERS_DEFAULT_ERROR_DESC
#define FORS_MF_PARAMETERS_FTOL_DESC                     MF_PARAMETERS_FTOL_DESC
#define FORS_MF_PARAMETERS_XTOL_DESC                     MF_PARAMETERS_XTOL_DESC
#define FORS_MF_PARAMETERS_FIT_CONTINUUM_DESC            MF_PARAMETERS_FIT_CONTINUUM_DESC
#define FORS_MF_PARAMETERS_CONTINUUM_N_DESC              MF_PARAMETERS_CONTINUUM_N_DESC
#define FORS_MF_PARAMETERS_FIT_WLC_DESC                  MF_PARAMETERS_FIT_WLC_DESC
#define FORS_MF_PARAMETERS_WLC_N_DESC                    MF_PARAMETERS_WLC_N_DESC
#define FORS_MF_PARAMETERS_WLC_CONST_DESC                MF_PARAMETERS_WLC_CONST_DESC
#define FORS_MF_PARAMETERS_FIT_RES_BOX_DESC              MF_PARAMETERS_FIT_RES_BOX_DESC
#define FORS_MF_PARAMETERS_FIT_GAUSS_DESC                MF_PARAMETERS_FIT_GAUSS_DESC
#define FORS_MF_PARAMETERS_RES_GAUSS_DESC                MF_PARAMETERS_RES_GAUSS_DESC
#define FORS_MF_PARAMETERS_FIT_LORENTZ_DESC              MF_PARAMETERS_FIT_LORENTZ_DESC
#define FORS_MF_PARAMETERS_RES_LORENTZ_DESC              MF_PARAMETERS_RES_LORENTZ_DESC
#define FORS_MF_PARAMETERS_KERN_MODE_DESC                MF_PARAMETERS_KERN_MODE_DESC
#define FORS_MF_PARAMETERS_KERN_FAC_DESC                 MF_PARAMETERS_KERN_FAC_DESC
#define FORS_MF_PARAMETERS_VAR_KERN_DESC                 MF_PARAMETERS_VAR_KERN_DESC
#define FORS_MOLECFIT_PARAMETER_USE_INPUT_KERNEL_DESC    MOLECFIT_PARAMETER_USE_INPUT_KERNEL_DESC
#define FORS_MF_PARAMETERS_FIT_TELESCOPE_BACK_DESC       MF_PARAMETERS_FIT_TELESCOPE_BACK_DESC
#define FORS_MF_PARAMETERS_TELESCOPE_BACK_CONST_DESC     MF_PARAMETERS_TELESCOPE_BACK_CONST_DESC
#define FORS_MF_PARAMETERS_PWV_DESC                      MF_PARAMETERS_PWV_DESC


*/

/*----------------------------------------------------------------------------*/
/**
 *                 Global variables
 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 *                 Macros
 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 *                 Typedefs: Structured types
 */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 *                 Functions prototypes
 */
/*----------------------------------------------------------------------------*/
cpl_error_code fors_molecfit_correct_config(cpl_frameset *frameset, const cpl_parameterlist  *parlist, cpl_parameterlist* ilist, cpl_parameterlist* iframelist);
int fors_molecfit_correct(cpl_frameset *frameset, const cpl_parameterlist  *parlist);
//char* fors_corr_basename(const char* string);
/*cpl_error_code fors_molecfit_model_config(cpl_frameset *frameset,
		const cpl_parameterlist  *parlist,
		cpl_parameterlist* ilist,
		cpl_parameterlist* iframelist);

cpl_error_code fors_molecfit_model_spec_header_calcs(const char* fname,const char* arm, cpl_parameterlist* ilist);

cpl_error_code fors_molecfit_model_spec_data_calcs(mf_wrap_fits* data, const char* is_idp, cpl_parameterlist* ilist,mf_wrap_model_parameter* parameters);

cpl_error_code fors_molecfit_setup_frameset(cpl_frameset* frameset,cpl_parameterlist* list,const char* arm,const char* input_name);
*/

#endif /*FORS_MOLECFIT_CORRECT_H*/
