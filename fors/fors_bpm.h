/*
 * This file is part of the FORS Data Reduction Pipeline
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifndef FORS_BPM_H_
#define FORS_BPM_H_

#include "cpl_image.h"
#include "cpl_mask.h"
#include "fors_image.h"


cpl_image * fors_bpm_create_combined_bpm
(cpl_mask ** non_linear_flat_masks,
 cpl_mask ** saturated_flat_masks,
 size_t nmasks);

cpl_image * fors_bpm_create_combined_bpm
(cpl_mask * non_linear_flat_mask,
 cpl_mask * saturated_flat_mask);

void fors_bpm_image_list_make_explicit(fors_image_list * ima_list);

void fors_bpm_image_make_explicit(const fors_image * ima);

#endif /* FORS_BPM_H_ */
