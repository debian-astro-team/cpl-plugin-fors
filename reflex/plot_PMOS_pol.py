import matplotlib.pyplot as plt
import numpy as np
from astropy.io import fits
import itertools
import sys

# Plot rows in a FORS2 PMOS results file
#
# Opening the FITS file reurns the data as a numpy ndarray
# accessing a row/column can be done by slicing
#
# Note that, like C (and unlike Fortran), Python is 0-indexed and the
# indices have the slowest axis first and fastest changing axis last;
# that is, for a 2D image, the fast axis (X-axis) which corresponds to
# the FITS NAXIS1 keyword, is the second index. Similarly, the
# 1-indexed subsection of x=11 to 20 (inclusive) and y=31 to 40
# (inclusive) would be given in Python as: data[30:40, 10:20]

# define wavelength scale along x-axis
def xaxis(start, step, num):
    return np.fromiter(itertools.count(start, step), float, num)

hdu1 = fits.open(sys.argv[1])
hdu2 = fits.open(sys.argv[2])
if len(sys.argv) >3:
    nrow = int(sys.argv[3])
else:
    sys.argv= sys.argv + [""]
    sys.argv[3] = '1'
    nrow = 1

imadat1 = hdu1[0].data
imadat2 = hdu2[0].data
stepx = hdu1[0].header['CD1_1']
startx = hdu1[0].header['CRPIX1']
startw = hdu1[0].header['CRVAL1']
obsname =  hdu1[0].header['HIERARCH ESO OBS NAME']
catg1 =  hdu1[0].header['HIERARCH ESO PRO CATG']
catg2 =  hdu2[0].header['HIERARCH ESO PRO CATG']
nx = hdu1[0].header['NAXIS1']
ny = hdu1[0].header['NAXIS2']
xref = xaxis(startw, stepx, nx)
xend = startw+(nx-1)*stepx

# extract rows from images
if nrow > 0:
    nrowlow = nrow-1
    row1 = imadat1[nrowlow:nrow,0:nx]
    row2 = imadat2[nrowlow:nrow,0:nx]
  
## convert row in 2d image into 1d array to match xref
row1x = np.reshape(row1, (nx,))
row2x = np.reshape(row2, (nx,))

# clear plot
plt.clf()
# set plot limits
if min(row2x) > min(row1x):
   miny = min(row1x)*0.9
else:
   miny = min(row2x)*0.9

if max(row2x) > max(row1x):
   maxy = max(row2x)*1.1
else:
   maxy = max(row1x)*1.1

plt.xlim=(startw,xend)
plt.ylim=(miny,maxy)



#axis labels
plt.xlabel('wavelength (Angstrom)')
plt.ylabel('polarization parameter')
label = obsname + " (row:" + sys.argv[3] + ")"
plt.annotate(label,xy=(90,60),xycoords='figure pixels',fontsize=12,color='k',
            horizontalalignment='left', verticalalignment='bottom',weight='bold')
plt.annotate(catg1,xy=(300,370),xycoords='figure pixels',fontsize=12,color='b',
             horizontalalignment='left', verticalalignment='bottom',weight='bold')
plt.annotate(catg2,xy=(300,350),xycoords='figure pixels',fontsize=12,color='r',
            horizontalalignment='left', verticalalignment='bottom',weight='bold')
#yposl = max(row1x)*0.95
#ypos1 = max(row1x)*0.87
#ypos2 = max(row1x)*0.79
#xpos = 0.6*startw+0.4*xend
#plt.annotate(label,xy=(xpos,yposl),fontsize=12,color='b',
#            horizontalalignment='left', verticalalignment='bottom',weight='bold')
#plt.annotate(catg1,xy=(xpos,ypos1),fontsize=12,color='k',
#             horizontalalignment='left', verticalalignment='bottom',weight='bold')
#plt.annotate(catg2,xy=(xpos,ypos2),fontsize=12,color='r',
#            horizontalalignment='left', verticalalignment='bottom',weight='bold')
plt.plot(xref,row1x,color='b')
plt.plot(xref,row2x,color='r')
plt.show()
plt.clf()
