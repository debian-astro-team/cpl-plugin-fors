import matplotlib.pyplot as plt
import numpy as np
from astropy.io import fits
import itertools
import sys
# <python> plot_PMOS_reduced_MEF.py <reduced_sci> <reduced_error> <reduced_sky>

# define wavelength scale along x-axis
def xaxis(start, step, num):
    return np.fromiter(itertools.count(start, step), float, num)
#print (len(sys.argv))

if len(sys.argv) <=4:
    sys.argv= sys.argv + [""]
    sys.argv[4] = '1'
ext = int(sys.argv[4])

if len(sys.argv) <=5:
    sys.argv= sys.argv + [""]
    sys.argv[5] = '1'
nrow = int(sys.argv[5])

reduced = fits.open(sys.argv[1])
reducedext = reduced[ext].data
catg =  reduced[0].header['HIERARCH ESO PRO CATG']
obsname =  reduced[0].header['HIERARCH ESO OBS NAME']

if sys.argv[2] != "?":
    error = fits.open(sys.argv[2])
    errorext = error[ext].data
    catgerr =  error[0].header['HIERARCH ESO PRO CATG']

if sys.argv[3] != "?":
   sky = fits.open(sys.argv[3])
   skyext = sky[ext].data
   catgsky =  sky[0].header['HIERARCH ESO PRO CATG']

stepx = reduced[ext].header['CD1_1']
startx = reduced[ext].header['CRPIX1']
startw = reduced[ext].header['CRVAL1']
nx = reduced[ext].header['NAXIS1']
ny = reduced[ext].header['NAXIS2']
xref = xaxis(startw, stepx, nx)
xend = startw+(nx-1)*stepx

# extract rows from images and
# convert row in 2d image into 1d array to match xref
if nrow > 0:
    nrowlow = nrow-1
    row1 = reducedext[nrowlow:nrow,0:nx]
    row1x = np.reshape(row1, (nx,))
    if sys.argv[2] != "?":
        row1e = errorext[nrowlow:nrow,0:nx]
        row1ex = np.reshape(row1e, (nx,))
    if sys.argv[3] != "?":
        row1s = skyext[nrowlow:nrow,0:nx]
        row1sx = np.reshape(row1s, (nx,))

# clear plot
plt.clf()
# set plot limits
plt.ylim(min(row1x)*0.9,max(row1x)*1.1)
plt.xlim=(startw,xend)
ypos0 = max(row1x)*0.05
ypos1 = max(row1x)
ypos2 = max(row1x)*0.94
ypos3 = max(row1x)*0.88
xposl = 0.99*startw+0.01*xend
xposr = 0.5*startw+0.5*xend

#axis labels
plt.xlabel('wavelength (Angstrom)')
plt.ylabel('spectrum')
label = "ext" + sys.argv[4] + " row" + sys.argv[5]
plt.plot(xref,row1x,color='b')
#plt.annotate(label,xy=(xposl,ypos1),fontsize=12,color='k',
#             horizontalalignment='left', verticalalignment='bottom',weight='bold')
#plt.annotate(catg,xy=(xposr,ypos1),fontsize=12,color='k',
#            horizontalalignment='left', verticalalignment='bottom',weight='bold')
if sys.argv[2] != "?":
    plt.plot(xref,row1ex,color='g')
#    plt.annotate(catgerr,xy=(xposr,ypos3),fontsize=12,color='g',
#            horizontalalignment='left', verticalalignment='bottom',weight='bold')
    plt.annotate(catgerr,xy=(300,350),xycoords='figure pixels',fontsize=12,color='g',
            horizontalalignment='left', verticalalignment='bottom',weight='bold')
if sys.argv[3] != "?":
    plt.plot(xref,row1sx,color='c')
#    plt.annotate(catgsky,xy=(xposr,ypos2),fontsize=12,color='c',
#            horizontalalignment='left', verticalalignment='bottom',weight='bold')
    plt.annotate(catgsky,xy=(300,370),xycoords='figure pixels',fontsize=12,color='c',
            horizontalalignment='left', verticalalignment='bottom',weight='bold')
plt.annotate(obsname,xy=(90,80),xycoords='figure pixels',fontsize=12,color='k',
             horizontalalignment='left', verticalalignment='bottom',weight='bold')
plt.annotate(label,xy=(90,60),xycoords='figure pixels',fontsize=12,color='k',
             horizontalalignment='left', verticalalignment='bottom',weight='bold')
plt.annotate(catg,xy=(300,390),xycoords='figure pixels',fontsize=12,color='b',
            horizontalalignment='left', verticalalignment='bottom',weight='bold')
plt.show()
plt.clf()
