Changes in 5.5.7
----------------
    Fixed PIPE-8960, PIPE-9131, PIPE-9167, PIPE-9227, PIPE-9233,
    PIPE-9296, PIPE-9312, PIPE-9369, PIPE-9375, PIPE-9399, PIPE-9426,
    PIPE-9446, PIPE-9447, PIPE-9454.
    Updated fors to use the head of the external mosca library
    Fixed compilation problems with XCode 12.5

Changes in 4.1.6
----------------

	Eliminate all memory leaks in fors_photometry_impl.c and 
	fors_photometry-test.c.

	In recipe fors_img_screen_flat, change computation of 
	QC.FLAT.PHN and QC.FLAT.CONAD, keeping into account 
	differences in illumination between first and second 
	raw flats.

	In recipe fors_bias, change computation of QC.BIAS.FPN
	according to the parameter definition given in the QC
	Dictionary. This change has also an impact on QC.BIAS.STRUCT.

	In recipe fors_zeropoint, add new parameter QC.EXTCOEFFERR.

        In recipe fors_img_screen_flat, add the possibility to 
	remove the large scale trend on screen flat by polynomial 
	fitting, as an alternative to heavy median smoothing.
	The method is much faster, and probably more appropriate
	than the old one.


Changes in 4.1.1
----------------

	Full version of fors_photometry.

	Fixed error propagation in fors_zeropoint to handle the correlated
	errors from the color coefficient.

	ALIGNED_PHOT tables: Replaced column FLAG (whether a star was used
	in the zeropoint computation) with column WEIGHT (the actual
	weight of a star in zeropoint computation, 0 if not used)
	

Changes in 4.1.0
----------------

	Implemented fors_photometry recipe first version.
	
	Added SExtractor output table as product of fors_img_science and
	fors_zeropoint.
	
        Revived QC parameter QC.MBIAS.LEVEL.

	Reject 50 ADU outliers in difference image when computing
	QC.RON. This causes a decrease in QC.RON.

	Include correction factor depending on the number of frames (to
	take into account the different statistics of the median/average) when
	computing QC.RONEXP. This causes QC.MBIAS.NRATIO to be closer to 1
	and for the right reasons.

	Added OBJECT column (standard star ID) to ALIGNED_PHOT table.	

	Added FLAG column (whether star was used for zeropoint computation)
	to ALIGNED_PHOT table.
	
	Normalize master flatfield to median=1 immediately before
	flatfielding (whether or not the flatfield was already normalized)

	Set QC.EXTCOEFF to zero when it cannot be computed (instead of
	computing an (even more) insensible value).

	Removed QC parameters from all but one FITS header per recipe.

	Propagate errors of color terms, extinction coefficients, expected
	zeropoint. Compute error of derived extinction coefficient.

	Made work better the post-SExtraction rejection of false
	detections at the border between illuminated, non-illuminated
	areas (necessary due to SExtractors imperfect background estimation).
	
	Changed default SExtraction method from MAG_AUTO to MAG_APER.
