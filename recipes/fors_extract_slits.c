/* $Id: fors_extract_slits.c,v 1.8 2013-08-20 17:02:58 cgarcia Exp $
 *
 * This file is part of the FORS Data Reduction Pipeline
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-08-20 17:02:58 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <cpl.h>
#include <moses.h>
#include <fors_dfs.h>

static int fors_extract_slits_create(cpl_plugin *);
static int fors_extract_slits_exec(cpl_plugin *);
static int fors_extract_slits_destroy(cpl_plugin *);
static int fors_extract_slits(cpl_parameterlist *, cpl_frameset *);

static char fors_extract_slits_description[] =
"This recipe is used to extract MOS/MXU slit spectra, following their\n"
"curvature, and to remap them into a spatially rectified image.\n"
"Please refer to the FORS Pipeline User's Manual for details about\n"
"the spectra remapping technique. Note however that the interpolation\n"
"is done exclusively along the spatial direction, and therefore the\n"
"output rectified image will have the same x size of the input spectral\n" 
"image.\n"
"\n"
"In the table below the MXU acronym can be alternatively read as MOS.\n\n"
"Input files:\n\n"
"  DO category:               Type:       Explanation:         Required:\n"
"  LAMP_UNBIAS_MXU\n"
"  or SCIENCE_UNBIAS_MXU\n"
"  or SCIENCE_UNFLAT_MXU\n"
"  or STANDARD_UNBIAS_MXU\n"
"  or STANDARD_UNFLAT_MXU\n"
"  or UNMAPPED_SCI_MXU\n"
"  or UNMAPPED_STD_MXU\n"
"  or UNMAPPED_SKY_SCI_MXU\n"
"  or UNMAPPED_SKY_STD_MXU    Calib       Spectral frame          Y\n"
"  SLIT_LOCATION_DETECT_MXU\n"
"  or SLIT_LOCATION_MXU       Calib       Master flat frame       Y\n"
"  CURV_COEFF_MXU             Calib       Spectral curvature      Y\n"
"  GRISM_TABLE                Calib       Grism table             .\n\n"
"Output files:\n\n"
"  DO category:               Data type:  Explanation:\n"
"  RECTIFIED_LAMP_MXU\n"
"  or RECTIFIED_ALL_SCI_MXU\n"
"  or RECTIFIED_ALL_STD_MXU\n"
"  or RECTIFIED_SCI_MXU\n"
"  or RECTIFIED_STD_MXU\n"
"  or RECTIFIED_SKY_SCI_MXU\n"
"  or RECTIFIED_SKY_STD_MXU   FITS image  Rectified slit spectra\n\n";

#define fors_extract_slits_exit(message)      \
{                                             \
if ((const char *)message != NULL) cpl_msg_error(recipe, message);  \
cpl_image_delete(spectra);                    \
cpl_image_delete(spatial);                    \
cpl_table_delete(grism_table);                \
cpl_table_delete(maskslits);                  \
cpl_table_delete(slits);                      \
cpl_table_delete(polytraces);                 \
cpl_propertylist_delete(header);              \
cpl_msg_indent_less();                        \
return -1;                                    \
}

#define fors_extract_slits_exit_memcheck(message)  \
{                                               \
if ((const char *)message != NULL) cpl_msg_info(recipe, message);     \
printf("free spectra (%p)\n", spectra);         \
cpl_image_delete(spectra);                      \
printf("free spatial (%p)\n", spatial);         \
cpl_image_delete(spatial);                      \
printf("free grism_table (%p)\n", grism_table); \
cpl_table_delete(grism_table);                  \
printf("free maskslits (%p)\n", maskslits);     \
cpl_table_delete(maskslits);                    \
printf("free slits (%p)\n", slits);             \
cpl_table_delete(slits);                        \
printf("free polytraces (%p)\n", polytraces);   \
cpl_table_delete(polytraces);                   \
printf("free header (%p)\n", header);           \
cpl_propertylist_delete(header);                \
cpl_msg_indent_less();                          \
return 0;                                       \
}


/**
 * @brief    Build the list of available plugins, for this module. 
 *
 * @param    list    The plugin list
 *
 * @return   0 if everything is ok, -1 otherwise
 *
 * Create the recipe instance and make it available to the application 
 * using the interface. This function is exported.
 */

int cpl_plugin_get_info(cpl_pluginlist *list)
{
    cpl_recipe *recipe = cpl_calloc(1, sizeof *recipe );
    cpl_plugin *plugin = &recipe->interface;

    cpl_plugin_init(plugin,
                    CPL_PLUGIN_API,
                    FORS_BINARY_VERSION,
                    CPL_PLUGIN_TYPE_RECIPE,
                    "fors_extract_slits",
                    "Spatial rectification of spectral image",
                    fors_extract_slits_description,
                    "Carlo Izzo",
                    PACKAGE_BUGREPORT,
    "This file is currently part of the FORS Instrument Pipeline\n"
    "Copyright (C) 2002-2010 European Southern Observatory\n\n"
    "This program is free software; you can redistribute it and/or modify\n"
    "it under the terms of the GNU General Public License as published by\n"
    "the Free Software Foundation; either version 2 of the License, or\n"
    "(at your option) any later version.\n\n"
    "This program is distributed in the hope that it will be useful,\n"
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\n"
    "GNU General Public License for more details.\n\n"
    "You should have received a copy of the GNU General Public License\n"
    "along with this program; if not, write to the Free Software Foundation,\n"
    "Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA\n",
                    fors_extract_slits_create,
                    fors_extract_slits_exec,
                    fors_extract_slits_destroy);

    cpl_pluginlist_append(list, plugin);
    
    return 0;
}


/**
 * @brief    Setup the recipe options    
 *
 * @param    plugin  The plugin
 *
 * @return   0 if everything is ok
 *
 * Defining the command-line/configuration parameters for the recipe.
 */

static int fors_extract_slits_create(cpl_plugin *plugin)
{
    cpl_recipe    *recipe;
    cpl_parameter *p;

    /* 
     * Check that the plugin is part of a valid recipe 
     */

    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin;
    else 
        return -1;

    /* 
     * Create the (empty) parameters list in the cpl_recipe object 
     */

    recipe->parameters = cpl_parameterlist_new(); 

    /*
     * Dispersion
     */

    p = cpl_parameter_new_value("fors.fors_extract_slits.dispersion",
                                CPL_TYPE_DOUBLE,
                                "Expected spectral dispersion (Angstrom/pixel)",
                                "fors.fors_extract_slits",
                                0.0);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "dispersion");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);

    /*
     * Start wavelength for spectral extraction
     */

    p = cpl_parameter_new_value("fors.fors_extract_slits.startwavelength",
                                CPL_TYPE_DOUBLE,
                                "Start wavelength in spectral extraction",
                                "fors.fors_extract_slits",
                                0.0);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "startwavelength");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);

    /*
     * End wavelength for spectral extraction
     */

    p = cpl_parameter_new_value("fors.fors_extract_slits.endwavelength",
                                CPL_TYPE_DOUBLE,
                                "End wavelength in spectral extraction",
                                "fors.fors_extract_slits",
                                0.0);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "endwavelength");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);

    /*
     * Flux conservation
     */
 
    p = cpl_parameter_new_value("fors.fors_extract_slits.flux",
                                CPL_TYPE_BOOL,
                                "Apply flux conservation",
                                "fors.fors_extract_slits",
                                TRUE);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "flux");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);

    return 0;
}


/**
 * @brief    Execute the plugin instance given by the interface
 *
 * @param    plugin  the plugin
 *
 * @return   0 if everything is ok
 */

static int fors_extract_slits_exec(cpl_plugin *plugin)
{
    cpl_recipe *recipe;
    
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin;
    else 
        return -1;

    return fors_extract_slits(recipe->parameters, recipe->frames);
}


/**
 * @brief    Destroy what has been created by the 'create' function
 *
 * @param    plugin  The plugin
 *
 * @return   0 if everything is ok
 */

static int fors_extract_slits_destroy(cpl_plugin *plugin)
{
    cpl_recipe *recipe;
    
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin;
    else 
        return -1;

    cpl_parameterlist_delete(recipe->parameters); 

    return 0;
}


/**
 * @brief    Interpret the command line options and execute the data processing
 *
 * @param    parlist     The parameters list
 * @param    frameset    The set-of-frames
 *
 * @return   0 if everything is ok
 */

static int fors_extract_slits(cpl_parameterlist *parlist, 
                               cpl_frameset *frameset)
{

    const char *recipe = "fors_extract_slits";


    /*
     * Input parameters
     */

    double      dispersion;
    double      startwavelength;
    double      endwavelength;
    int         flux;

    /*
     * CPL objects
     */

    cpl_image        *spectra     = NULL;
    cpl_image        *spatial     = NULL;
    cpl_table        *grism_table = NULL;
    cpl_table        *slits       = NULL;
    cpl_table        *polytraces  = NULL;
    cpl_table        *maskslits   = NULL;
    cpl_propertylist *header      = NULL;

    /*
     * Auxiliary variables
     */

    char        version[80];
    const char *input_tag;
    const char *output_tag;
    const char *slit_location_tag;
    const char *curv_coeff_tag;
    int         nframes;
    int         rebin;
    int         treat_as_lss;
    double      reference;
    double      mxpos;
    int         mxu, mos, lss;
    int         slit_l, slit_d;
    int         lamp_mxu;
    int         lamp_mos;
    int         lamp_lss;
    int         scib_mxu;
    int         scib_mos;
    int         scib_lss;
    int         scif_mxu;
    int         scif_mos;
    int         scif_lss;
    int         stab_mxu;
    int         stab_mos;
    int         stab_lss;
    int         staf_mxu;
    int         staf_mos;
    int         staf_lss;
    int         sciu_mxu;
    int         sciu_mos;
    int         sciu_lss;
    int         stau_mxu;
    int         stau_mos;
    int         stau_lss;
    int         scis_mxu;
    int         scis_mos;
    int         scis_lss;
    int         stas_mxu;
    int         stas_mos;
    int         stas_lss;
    int         nslits_out_det = 0;

    char       *instrume = NULL;


    cpl_msg_set_indentation(2);


    /*
     * Get configuration parameters
     */

    cpl_msg_info(recipe, "Recipe %s configuration parameters:", recipe);
    cpl_msg_indent_more();
    
    if (cpl_frameset_count_tags(frameset, "GRISM_TABLE") > 1)
        fors_extract_slits_exit("Too many in input: GRISM_TABLE"); 

    grism_table = dfs_load_table(frameset, "GRISM_TABLE", 1);

    dispersion = dfs_get_parameter_double(parlist,
                    "fors.fors_extract_slits.dispersion", grism_table);

    if (dispersion <= 0.0)
        fors_extract_slits_exit("Invalid spectral dispersion value");

    startwavelength = dfs_get_parameter_double(parlist,
                    "fors.fors_extract_slits.startwavelength", grism_table);
    if (startwavelength > 1.0)
        if (startwavelength < 3000.0 || startwavelength > 13000.0)
            fors_extract_slits_exit("Invalid wavelength");

    endwavelength = dfs_get_parameter_double(parlist,
                    "fors.fors_extract_slits.endwavelength", grism_table);
    if (endwavelength > 1.0) {
        if (endwavelength < 3000.0 || endwavelength > 13000.0)
            fors_extract_slits_exit("Invalid wavelength");
        if (startwavelength < 1.0)
            fors_extract_slits_exit("Invalid wavelength interval");
    }

    if (startwavelength > 1.0)
        if (endwavelength - startwavelength <= 0.0)
            fors_extract_slits_exit("Invalid wavelength interval");

    flux = dfs_get_parameter_bool(parlist, 
                                  "fors.fors_extract_slits.flux", NULL);

    cpl_table_delete(grism_table); grism_table = NULL;

    if (cpl_error_get_code())
        fors_extract_slits_exit("Failure reading the configuration parameters");


    cpl_msg_indent_less();
    cpl_msg_info(recipe, "Check input set-of-frames:");
    cpl_msg_indent_more();

    mxu  = lamp_mxu = cpl_frameset_count_tags(frameset, "LAMP_UNBIAS_MXU");
    mos  = lamp_mos = cpl_frameset_count_tags(frameset, "LAMP_UNBIAS_MOS");
    lss  = lamp_lss = cpl_frameset_count_tags(frameset, "LAMP_UNBIAS_LSS");
    mxu += scib_mxu = cpl_frameset_count_tags(frameset, "SCIENCE_UNBIAS_MXU");
    mos += scib_mos = cpl_frameset_count_tags(frameset, "SCIENCE_UNBIAS_MOS");
    lss += scib_lss = cpl_frameset_count_tags(frameset, "SCIENCE_UNBIAS_LSS");
    mxu += scif_mxu = cpl_frameset_count_tags(frameset, "SCIENCE_UNFLAT_MXU");
    mos += scif_mos = cpl_frameset_count_tags(frameset, "SCIENCE_UNFLAT_MOS");
    lss += scif_lss = cpl_frameset_count_tags(frameset, "SCIENCE_UNFLAT_LSS");
    mxu += stab_mxu = cpl_frameset_count_tags(frameset, "STANDARD_UNBIAS_MXU");
    mos += stab_mos = cpl_frameset_count_tags(frameset, "STANDARD_UNBIAS_MOS");
    lss += stab_lss = cpl_frameset_count_tags(frameset, "STANDARD_UNBIAS_LSS");
    mxu += staf_mxu = cpl_frameset_count_tags(frameset, "STANDARD_UNFLAT_MXU");
    mos += staf_mos = cpl_frameset_count_tags(frameset, "STANDARD_UNFLAT_MOS");
    lss += staf_lss = cpl_frameset_count_tags(frameset, "STANDARD_UNFLAT_LSS");
    mxu += sciu_mxu = cpl_frameset_count_tags(frameset, "UNMAPPED_SCI_MXU");
    mos += sciu_mos = cpl_frameset_count_tags(frameset, "UNMAPPED_SCI_MOS");
    lss += sciu_lss = cpl_frameset_count_tags(frameset, "UNMAPPED_SCI_LSS");
    mxu += stau_mxu = cpl_frameset_count_tags(frameset, "UNMAPPED_STD_MXU");
    mos += stau_mos = cpl_frameset_count_tags(frameset, "UNMAPPED_STD_MOS");
    lss += stau_lss = cpl_frameset_count_tags(frameset, "UNMAPPED_STD_LSS");
    mxu += scis_mxu = cpl_frameset_count_tags(frameset, "UNMAPPED_SKY_SCI_MXU");
    mos += scis_mos = cpl_frameset_count_tags(frameset, "UNMAPPED_SKY_SCI_MOS");
    lss += scis_lss = cpl_frameset_count_tags(frameset, "UNMAPPED_SKY_SCI_LSS");
    mxu += stas_mxu = cpl_frameset_count_tags(frameset, "UNMAPPED_SKY_STD_MXU");
    mos += stas_mos = cpl_frameset_count_tags(frameset, "UNMAPPED_SKY_STD_MOS");
    lss += stas_lss = cpl_frameset_count_tags(frameset, "UNMAPPED_SKY_STD_LSS");

    nframes = mos + mxu + lss;

    if (nframes == 0) {
        fors_extract_slits_exit("Missing input spectral frame");
    }
    if (nframes > 1) {
        cpl_msg_error(recipe, 
                      "Too many input spectral frames (%d > 1)", nframes);
        fors_extract_slits_exit(NULL);
    }

    if (lss)
        fors_extract_slits_exit("Use this recipe just with MOS/MXU data.");

    if (mxu) {
        slit_l = cpl_frameset_count_tags(frameset, "SLIT_LOCATION_MXU");
        slit_d = cpl_frameset_count_tags(frameset, "SLIT_LOCATION_DETECT_MXU");
    }
    else {
        slit_l = cpl_frameset_count_tags(frameset, "SLIT_LOCATION_MOS");
        slit_d = cpl_frameset_count_tags(frameset, "SLIT_LOCATION_DETECT_MOS");
    }

    nframes = slit_l + slit_d;

    if (nframes == 0) {
        fors_extract_slits_exit("Missing input slit location table");
    }
    if (nframes > 1) {
        cpl_msg_error(recipe,
                      "Too many input slit location tables (%d > 1)", nframes);
        fors_extract_slits_exit(NULL);
    }

    if (slit_l) {
        if (mxu)
            slit_location_tag = "SLIT_LOCATION_MXU";
        else
            slit_location_tag = "SLIT_LOCATION_MOS";
    }
    else {
        if (mxu)
            slit_location_tag = "SLIT_LOCATION_DETECT_MXU";
        else
            slit_location_tag = "SLIT_LOCATION_DETECT_MOS";
    }

    if (mxu)
        curv_coeff_tag = "CURV_COEFF_MXU";
    else
        curv_coeff_tag = "CURV_COEFF_MOS";

    if (lamp_mxu) {
        input_tag = "LAMP_UNBIAS_MXU";
        output_tag = "RECTIFIED_LAMP_MXU";
    }
    else if (lamp_mos) {
        input_tag = "LAMP_UNBIAS_MOS";
        output_tag = "RECTIFIED_LAMP_MOS";
    }
    else if (scib_mxu) {
        input_tag = "SCIENCE_UNBIAS_MXU";
        output_tag = "RECTIFIED_ALL_SCI_MXU";
    }
    else if (scib_mos) {
        input_tag = "SCIENCE_UNBIAS_MOS";
        output_tag = "RECTIFIED_ALL_SCI_MOS";
    }
    else if (scif_mxu) {
        input_tag = "SCIENCE_UNFLAT_MXU";
        output_tag = "RECTIFIED_ALL_SCI_MXU";
    }
    else if (scif_mos) {
        input_tag = "SCIENCE_UNFLAT_MOS";
        output_tag = "RECTIFIED_ALL_SCI_MOS";
    }
    else if (stab_mxu) {
        input_tag = "STANDARD_UNBIAS_MXU";
        output_tag = "RECTIFIED_ALL_STD_MXU";
    }
    else if (stab_mos) {
        input_tag = "STANDARD_UNBIAS_MOS";
        output_tag = "RECTIFIED_ALL_STD_MOS";
    }
    else if (staf_mxu) {
        input_tag = "STANDARD_UNFLAT_MXU";
        output_tag = "RECTIFIED_ALL_STD_MXU";
    }
    else if (staf_mos) {
        input_tag = "STANDARD_UNFLAT_MOS";
        output_tag = "RECTIFIED_ALL_STD_MOS";
    }
    else if (sciu_mxu) {
        input_tag = "UNMAPPED_SCI_MXU";
        output_tag = "RECTIFIED_SCI_MXU";
    }
    else if (sciu_mos) {
        input_tag = "UNMAPPED_SCI_MOS";
        output_tag = "RECTIFIED_SCI_MOS";
    }
    else if (stau_mxu) {
        input_tag = "UNMAPPED_STD_MXU";
        output_tag = "RECTIFIED_STD_MXU";
    }
    else if (stau_mos) {
        input_tag = "UNMAPPED_STD_MOS";
        output_tag = "RECTIFIED_STD_MOS";
    }
    else if (scis_mxu) {
        input_tag = "UNMAPPED_SKY_SCI_MXU";
        output_tag = "RECTIFIED_SKY_SCI_MXU";
    }
    else if (scis_mos) {
        input_tag = "UNMAPPED_SKY_SCI_MOS";
        output_tag = "RECTIFIED_SKY_SCI_MOS";
    }
    else if (stas_mxu) {
        input_tag = "UNMAPPED_SKY_STD_MXU";
        output_tag = "RECTIFIED_SKY_STD_MXU";
    }
    else if (stas_mos) {
        input_tag = "UNMAPPED_SKY_STD_MOS";
        output_tag = "RECTIFIED_SKY_STD_MOS";
    }

    header = dfs_load_header(frameset, input_tag, 0);

    if (header == NULL)
        fors_extract_slits_exit("Cannot load master flat frame header");

    if (mos)
        maskslits = mos_load_slits_fors_mos(header, &nslits_out_det);
    else
        maskslits = mos_load_slits_fors_mxu(header);

    /*
     * Check if all slits have the same X offset: in such case, abort!
     */

    treat_as_lss = fors_mos_is_lss_like(maskslits, nslits_out_det);

    cpl_table_delete(maskslits); maskslits = NULL;

    if (treat_as_lss) {
        cpl_msg_error(recipe, "All slits have the same offset: %.2f mm\n"
                      "The LSS data reduction strategy must be applied.", 
                      mxpos);
        fors_extract_slits_exit(NULL);
    }

    if (!dfs_equal_keyword(frameset, "ESO INS GRIS1 ID"))
        fors_extract_slits_exit("Input frames are not from the same grism");

    if (!dfs_equal_keyword(frameset, "ESO INS FILT1 ID"))
        fors_extract_slits_exit("Input frames are not from the same filter");

    if (!dfs_equal_keyword(frameset, "ESO DET CHIP1 ID"))
        fors_extract_slits_exit("Input frames are not from the same chip");


    /*
     * Get the reference wavelength and the rebin factor along the
     * dispersion direction from the master flat frame
     */

    instrume = (char *)cpl_propertylist_get_string(header, "INSTRUME");
    if (instrume == NULL)
        fors_extract_slits_exit("Missing keyword INSTRUME in master "
                                 "flat header");

    if (instrume[4] == '1')
        snprintf(version, 80, "%s/%s", "fors1", VERSION);
    if (instrume[4] == '2')
        snprintf(version, 80, "%s/%s", "fors2", VERSION);

    reference = cpl_propertylist_get_double(header, "ESO INS GRIS1 WLEN");

    if (cpl_error_get_code() != CPL_ERROR_NONE)
        fors_extract_slits_exit("Missing keyword ESO INS GRIS1 WLEN "
                                 "in master flat frame header");

    if (reference < 3000.0)   /* Perhaps in nanometers... */
        reference *= 10;

    if (reference < 3000.0 || reference > 13000.0) {
        cpl_msg_error(recipe, "Invalid central wavelength %.2f read from "
                      "keyword ESO INS GRIS1 WLEN in master flat header",
                      reference);
        fors_extract_slits_exit(NULL);
    }

    cpl_msg_info(recipe, "The central wavelength is: %.2f", reference);

    rebin = cpl_propertylist_get_int(header, "ESO DET WIN1 BINX");

    if (cpl_error_get_code() != CPL_ERROR_NONE)
        fors_extract_slits_exit("Missing keyword ESO DET WIN1 BINX "
                                 "in master flat header");

    if (rebin != 1) {
        dispersion *= rebin;
        cpl_msg_warning(recipe, "The rebin factor is %d, and therefore the "
                        "working dispersion used is %f A/pixel", rebin,
                        dispersion);
    }

    cpl_msg_indent_less();
    cpl_msg_info(recipe, "Load input frames...");
    cpl_msg_indent_more();

    spectra = dfs_load_image(frameset, input_tag, CPL_TYPE_FLOAT, 0, 0);
    if (spectra == NULL)
        fors_extract_slits_exit("Cannot load input spectral frame");

    slits = dfs_load_table(frameset, slit_location_tag, 1);
    if (slits == NULL)
        fors_extract_slits_exit("Cannot load slits location table");

    polytraces = dfs_load_table(frameset, curv_coeff_tag, 1);
    if (slits == NULL)
        fors_extract_slits_exit("Cannot load spectral curvature table");

    spatial = mos_spatial_calibration(spectra, slits, polytraces, reference,
                                      startwavelength, endwavelength,
                                      dispersion, flux, NULL);

    cpl_image_delete(spectra); spectra = NULL;
    cpl_table_delete(polytraces); polytraces = NULL;
    cpl_table_delete(slits); slits = NULL;

    cpl_propertylist_delete(header); header = NULL;
    header = cpl_propertylist_new();

    cpl_propertylist_update_double(header, "CRPIX2", 1.0);
    cpl_propertylist_update_double(header, "CRVAL2", 1.0);
    /* cpl_propertylist_update_double(header, "CDELT2", 1.0); */
    cpl_propertylist_update_double(header, "CD1_1", 1.0);
    cpl_propertylist_update_double(header, "CD1_2", 0.0);
    cpl_propertylist_update_double(header, "CD2_1", 0.0);
    cpl_propertylist_update_double(header, "CD2_2", 1.0);
    cpl_propertylist_update_string(header, "CTYPE1", "LINEAR");
    cpl_propertylist_update_string(header, "CTYPE2", "PIXEL");

    if (dfs_save_image(frameset, spatial, output_tag,
                       header, parlist, recipe, version))
        fors_extract_slits_exit(NULL);

    cpl_image_delete(spatial); spatial = NULL;
    cpl_propertylist_delete(header); header = NULL;

    return 0;
}
