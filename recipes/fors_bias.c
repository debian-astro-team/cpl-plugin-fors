/* $Id: fors_bias.c,v 1.9 2011-10-12 14:59:57 cgarcia Exp $
 *
 * This file is part of the FORS Data Reduction Pipeline
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2011-10-12 14:59:57 $
 * $Revision: 1.9 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_bias_impl.h>
#include <fors_dfs.h>
#include <fors_utils.h>

#include <cpl.h>

static int fors_bias_create(cpl_plugin *);
static int fors_bias_exec(cpl_plugin *);
static int fors_bias_destroy(cpl_plugin *);

/**
 * @defgroup fors_bias fors_recipe Recipe
 *
 * See recipe description for details.
 */

/**@{*/


/**
 * @brief    Build the list of available plugins, for this module. 
 *
 * @param    list    The plugin list
 *
 * @return   0 if everything is ok, -1 otherwise
 *
 * Create the recipe instance and make it available to the application 
 * using the interface. This function is exported.
 */

int cpl_plugin_get_info(cpl_pluginlist *list)
{
    cpl_recipe *recipe = cpl_calloc(1, sizeof *recipe );
    cpl_plugin *plugin = &recipe->interface;

    if (FORS_BINARY_VERSION != fors_get_version_binary())
    {
        cpl_msg_error(cpl_func, 
              "I am fors_bias version %d, but I am linking "
              "against the FORS library version %d. "
              "This will not work. "
              "Please remove all previous installations "
              "of the " PACKAGE_NAME " and try again.",
              FORS_BINARY_VERSION, fors_get_version_binary());
        return 1;
    }

    cpl_plugin_init(plugin,
                    CPL_PLUGIN_API,
                    FORS_BINARY_VERSION,
                    CPL_PLUGIN_TYPE_RECIPE,
                    fors_bias_name,
                    fors_bias_description_short,
                    fors_bias_description,
                    fors_bias_author,
                    fors_bias_email,
                    fors_get_license(),
                    fors_bias_create,
                    fors_bias_exec,
                    fors_bias_destroy);

    cpl_pluginlist_append(list, plugin);
    
    return 0;
}


/**
 * @brief    Setup the recipe options    
 *
 * @param    plugin  The plugin
 *
 * @return   0 if everything is ok
 *
 * Defining the command-line/configuration parameters for the recipe.
 */

static int fors_bias_create(cpl_plugin *plugin)
{
    cpl_recipe    *recipe;

    if (cpl_error_get_code() != CPL_ERROR_NONE) {
        cpl_msg_error(cpl_func, 
                      "CPL error code is set (%s), "
                      "refusing to create recipe fors_bias", 
                      cpl_error_get_message());
        return 1;
    }
    
    /* 
     * Check that the plugin is part of a valid recipe 
     */

    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) {
        recipe = (cpl_recipe *)plugin;
    }
    else {
        return 1;
    }

    /* 
     * Create the parameters list in the cpl_recipe object 
     */

    recipe->parameters = cpl_parameterlist_new(); 
    
    fors_bias_define_parameters(recipe->parameters);

    if (cpl_error_get_code() != CPL_ERROR_NONE) {
        cpl_msg_error(cpl_func, 
                      "Could not create fors_bias parameters");
        return 1;
    }
        
    return 0;
}


/**
 * @brief    Execute the plugin instance given by the interface
 *
 * @param    plugin  the plugin
 *
 * @return   0 if everything is ok
 */

static int fors_bias_exec(cpl_plugin *plugin)
{
    cpl_recipe *recipe;
    cpl_errorstate initial_errorstate = cpl_errorstate_get();

    if (cpl_error_get_code() != CPL_ERROR_NONE) {
        cpl_msg_error(cpl_func, 
                      "CPL error code is set (%s), "
                      "refusing to execute recipe fors_bias", 
                      cpl_error_get_message());
        return 1;
    }
    
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) {
        recipe = (cpl_recipe *)plugin;
    }
    else {
        return 1;
    }

    if (recipe->frames == NULL) {
        cpl_msg_error(cpl_func, 
                      "Null frameset");
        return 1;
    }

    if (recipe->parameters == NULL) {
        cpl_msg_error(cpl_func, 
                      "Null parameter list");
        return 1;
    }

    /* Issue a banner */
    fors_print_banner();

    fors_bias(recipe->frames, recipe->parameters);

    return fors_end(recipe->frames, initial_errorstate);
}


/**
 * @brief    Destroy what has been created by the 'create' function
 *
 * @param    plugin  The plugin
 *
 * @return   0 if everything is ok
 */

static int fors_bias_destroy(cpl_plugin *plugin)
{
    cpl_recipe *recipe;
    
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) {
        recipe = (cpl_recipe *)plugin;
    }
    else {
        return -1;
    }

    cpl_parameterlist_delete(recipe->parameters); 

    return 0;
}

/**@}*/
